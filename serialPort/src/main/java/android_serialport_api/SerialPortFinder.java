package android_serialport_api;


import android.util.Log;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.Vector;

/**
 * 作者 ：Wesley
 * 时间 ：2020-08-03 11:00
 * 这个类是干嘛的？：SerialPortFinder
 */
public class SerialPortFinder {
    private static final String TAG = SerialPortFinder.class.getSimpleName();
    private Vector<Driver> mDrivers = null;

    public SerialPortFinder() {
    }

    Vector<Driver> getDrivers() throws IOException {
        if (this.mDrivers == null) {
            this.mDrivers = new Vector();
            LineNumberReader r = new LineNumberReader(new FileReader("/proc/tty/drivers"));

            String l;
            while ((l = r.readLine()) != null) {
                String drivername = l.substring(0, 21).trim();
                String[] w = l.split(" +");
                if (w.length >= 5 && w[w.length - 1].equals("serial")) {
                    Log.d(TAG, "???? Found new driver " + drivername + " on " + w[w.length - 4]);
                    this.mDrivers.add(new Driver(drivername, w[w.length - 4]));
                }
            }

            r.close();
        }

        return this.mDrivers;
    }

    public String[] getAllDevices() {
        Vector devices = new Vector();

        try {
            Iterator itdriv = this.getDrivers().iterator();

            while (itdriv.hasNext()) {
                Driver driver = (Driver) itdriv.next();
                Vector<File> files = driver.getDevices();
                if (files == null) {
                    return null;
                }

                Iterator var5 = files.iterator();

                while (var5.hasNext()) {
                    File file = (File) var5.next();
                    String device = file.getName();
                    String value = String.format("%s (%s)", new Object[]{device, driver.getName()});
                    devices.add(value);
                }
            }
        } catch (IOException var9) {
            var9.printStackTrace();
        }

        return (String[]) devices.toArray(new String[devices.size()]);
    }

    public String[] getAllDevicesPath() {
        Vector devices = new Vector();

        try {
            Iterator itdriv = this.getDrivers().iterator();

            while (itdriv.hasNext()) {
                Driver driver = (Driver) itdriv.next();
                Vector<File> files = driver.getDevices();
                if (files == null) {
                    return null;
                }

                Iterator var5 = files.iterator();

                while (var5.hasNext()) {
                    File file = (File) var5.next();
                    String device = file.getAbsolutePath();
                    devices.add(device);
                }
            }
        } catch (IOException var8) {
            var8.printStackTrace();
        }

        return (String[]) devices.toArray(new String[devices.size()]);
    }

    public class Driver {
        private String mDriverName;
        private String mDeviceRoot;
        Vector<File> mDevices = null;

        public Driver(String name, String root) {
            this.mDriverName = name;
            this.mDeviceRoot = root;
        }

        public Vector<File> getDevices() {
            if (this.mDevices == null) {
                this.mDevices = new Vector();
                File dev = new File("/dev");
                File[] files = dev.listFiles();
                if (files == null) {
                    return null;
                }

                for (int i = 0; i < files.length; ++i) {
                    if (files[i].getAbsolutePath().startsWith(this.mDeviceRoot)) {
                        Log.d(SerialPortFinder.TAG, "???? Found new device: " + files[i]);
                        this.mDevices.add(files[i]);
                    }
                }
            }

            return this.mDevices;
        }

        public String getName() {
            return this.mDriverName;
        }
    }
}

