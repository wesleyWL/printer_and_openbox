package android_serialport_api;

import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

/**
 * 作者 ：Wesley
 * 时间 ：2020-08-03 11:00
 * 这个类是干嘛的？：SerialPort
 */
public class SerialPort {
    int fd;
    private static final String TAG = "SerialPort";
    private FileDescriptor mFd;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;

    public SerialPort(File device, int baudrate, int flags) throws SecurityException, IOException {
        Process su;
        //检查访问权限，如果没有读写权限，进行文件操作，修改文件访问权限
        if (!device.canRead() || !device.canWrite()) {
            try {
                //通过挂载到linux的方式，修改文件的操作权限
                su = Runtime.getRuntime().exec("/system/xbin/su");
                String cmd = "chmod 777 " + device.getAbsolutePath() + "\n" + "exit\n";
                su.getOutputStream().write(cmd.getBytes());

                if ((su.waitFor() != 0) || !device.canRead() || !device.canWrite()) {
                    throw new SecurityException();
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new SecurityException();
            }
        }

        mFd = open(device.getAbsolutePath(), baudrate, flags);

        if (mFd == null) {
            Log.e(TAG, "native open returns null");
            throw new IOException();
        } else {
            su = null;
            try {
                Field descriptor = this.mFd.getClass().getDeclaredField("descriptor");
                descriptor.setAccessible(true);
                this.fd = descriptor.getInt(this.mFd);
            } catch (NoSuchFieldException var6) {
                var6.printStackTrace();
            } catch (IllegalAccessException var7) {
                var7.printStackTrace();
            }
            this.mFd.sync();
            mFileInputStream = new FileInputStream(mFd);
            mFileOutputStream = new FileOutputStream(mFd);

        }
    }

    // Getters and setters
    public InputStream getInputStream() {
        return mFileInputStream;
    }

    public OutputStream getOutputStream() {
        return mFileOutputStream;
    }


    // JNI(调用java本地接口，实现串口的打开和关闭)

    /**
     * 串口有五个重要的参数：串口设备名，波特率，检验位，数据位，停止位
     * 其中检验位一般默认位NONE,数据位一般默认为8，停止位默认为1
     * @param path     串口设备的绝对路径
     * @param baudrate 波特率
     * @param flags    校验位
     */
    private native static FileDescriptor open(String path, int baudrate, int flags);

    public native void close();


    public native void flush(int var1);

    public native int write(int var1, byte[] var2, int var3, int var4);

    public int write(byte[] data, int offset, int size) {
        int write = this.write(this.fd, data, offset, size);
        return write;
    }

    public native void tcDrain();

    public native int getInfo(int var1);

    public int getInfo() {
        return this.getInfo(this.fd);
    }

    public native void tcFlow(int var1);


    static {//加载jni下的C文件库
        System.loadLibrary("serial_port_sangda");
        System.loadLibrary("serial_port");
    }
}

