package com.zbw.zbw_android_printing;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.zbw.printlibrary.activity.PrintNewUsbActivity;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.printConnector.NewPrint;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2023-06-21 14:25
 * 这个类是干嘛的？：PrintActivity
 */
public class PrintActivity extends Activity {
   private IPrinterControl mNewPrint;

   private boolean isConnect;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_print);
//      initPrint();
//      initUsbPrint();
      initPrint1();
      findViewById(R.id.connectBt).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            mNewPrint.connectPrinter();
         }
      });
      findViewById(R.id.print1Bt).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
//            if(!isConnect){
//               Toast.makeText(PrintActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
//               return;
//            }
            testPrint1();
         }
      });
      findViewById(R.id.print2Bt).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            if(!isConnect){
               Toast.makeText(PrintActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
               return;
            }
            testPrint2();
         }
      });
      findViewById(R.id.openBox).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            if(!isConnect){
               Toast.makeText(PrintActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
               return;
            }
            mNewPrint.openMoneyBox();
         }
      });


      findViewById(R.id.disConnectBt).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            mNewPrint.disConnectPrinter();
            isConnect = false;
         }
      });

      findViewById(R.id.settingBt).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            startActivity(new Intent(PrintActivity.this, PrintNewUsbActivity.class));
         }
      });

   }

   /**
    * 打印测试1
    */
   private void testPrint1(){
      List<String> testList = new ArrayList<>();
      testList.add("智百威购物广场");
      testList.add(" ");
      testList.add("标题2");
      testList.add("内容：123123123");
      testList.add("内容：123123123");
      testList.add("内容：123123123");
      testList.add("内容.ss");

      testList.add("内容：ss");
      testList.add("内容：ss");
      testList.add("\n");
      testList.add("\n");
      testList.add("\n");
      LinkedHashMap<Integer, Bitmap> map= new LinkedHashMap();
      map.put(0, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
      map.put(3, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
      map.put(6, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
      mNewPrint.printStringAndBitmap(testList,map);
   }

   /**
    * 打印测试2
    */
   private void testPrint2(){
      List<AttributeBean> data = new ArrayList<>();
      for (int i = 0; i < 10; i++) {
         AttributeBean info = new AttributeBean();
         info.setContext_type(0);
         info.setFont_gray(i%3);
         info.setFont_size(i%3);
         info.setContext("打印内容士大夫快速导航富士康受打印内容士大夫快速导航富士康受到激发函数"+i);
         if(i == 0 || i == 12){
            info.setContext("价格"+i);
            info.setFont_gray(1);
            info.setFont_size(2);
         }

         if (i == 1|| i == 13){
            info.setContext("100元");
            info.setFont_gray(2);
            info.setFont_size(0);
         }
//         if(i == 3|| i == 15){
//            info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//            info.setContext_type(1);
//            info.setFont_gray(1);
//            info.setFont_line(true);
//         }

//         if(i == 4|| i == 16){
//            info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//            info.setContext_type(1);
//            info.setFont_gray(1);
//            info.setFont_line(false);
//         }
//
//         if(i == 5|| i == 17){
//            info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//            info.setContext_type(1);
//            info.setFont_gray(2);
//         }

//         if(i == 8|| i == 20){
//            info.setContext_type(2);
//         }
         data.add(info);
      }

      data.add(new AttributeBean(""));
      data.add(new AttributeBean(""));
      data.add(new AttributeBean(""));
      data.add(new AttributeBean(""));
      mNewPrint.printString(data);
   }



   private void initPrint1(){
      mNewPrint = NewPrint.getInstance().NewControl(NewPrint.PRINT_RONGDA, this, new IPrintCallBack() {
         @Override
         public void onConnect(String message, boolean b) {
               Toast.makeText(PrintActivity.this,message,Toast.LENGTH_SHORT).show();
            isConnect = b;
         }

         @Override
         public void onDisConnect(String message, boolean b) {

         }

         @Override
         public void onPrintResult(boolean resultCode) {

         }
      });
   }


   private void initUsbPrint(){
      mNewPrint = NewPrint.getInstance().NewControlUSB(this, new IPrintCallBack() {
         @Override
         public void onConnect(String message, boolean b) {
            Toast.makeText(PrintActivity.this,message,Toast.LENGTH_SHORT).show();
            isConnect = b;
         }

         @Override
         public void onDisConnect(String message, boolean b) {

         }

         @Override
         public void onPrintResult(boolean resultCode) {

         }
      },4070,33054,NewPrint.PRINT_USB);

   }
   private void initPrint(){
      mNewPrint = NewPrint.getInstance().NewControl(NewPrint.PRINT_USB, this, new IPrintCallBack() {
         @Override
         public void onConnect(String message, boolean b) {
            Toast.makeText(PrintActivity.this,message,Toast.LENGTH_SHORT).show();
            isConnect = b;
         }

         @Override
         public void onDisConnect(String message, boolean b) {

         }

         @Override
         public void onPrintResult(boolean resultCode) {

         }
      });
   }
}
