package com.zbw.zbw_android_printing;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.zbw.printlibrary.activity.PrintBluetoothActivity;
import com.zbw.printlibrary.activity.PrintNewUsbActivity;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.printConnector.NewPrint;
import com.zbw.printlibrary.printConnector.netWorkInterface;
import com.zbw.printlibrary.util.PrintContentToBitmap;
import com.zbw.printlibrary.util.PrintSharedPreferencesUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MainActivity extends Activity {
    private netWorkInterface mIPrinterControl;
    private AttributeBean attributeBean;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = findViewById(R.id.image_1);

        //PrintSharedPreferencesUtils.setParam(this,"editTextIps","192.168.0.100");
        PrintSharedPreferencesUtils.setParam(this,"editTextIps","10.1.5.111");
        //PrintSharedPreferencesUtils.setParam(this,"editTextIps","10.1.5.111");
        PrintSharedPreferencesUtils.setParam(this,"editTextPorts","9100");
        //mIPrinterControl = NewPrint.getInstance().NewControl(NewPrint.PRINT_MEITUAN_HARDWARE,MainActivity.this
        NewPrint.paperSize = 58;
        mIPrinterControl = NewPrint.getInstance().NewNetControl(MainActivity.this
                ,new IPrintCallBack() {
                    @Override
                    public void onConnect(String message, boolean b) {
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDisConnect(String message, boolean b) {
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPrintResult(final boolean b) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (b){
                                    Toast.makeText(getApplicationContext(),"打印成功",Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getApplicationContext(),"打印失败",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                });

        findViewById(R.id.buttonBT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PrintBluetoothActivity.class));
            }
        });

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mIPrinterControl.connectPrinter();
//                mIPrinterControl.setDefaultBuzzer(true,8,1);
                startActivity(new Intent(MainActivity.this, PrintNewUsbActivity.class));
            }
        });

        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIPrinterControl.connectPrinter();
                List<String> testList = new ArrayList<>();
                testList.add("智百威购物广场");
                testList.add("标题2");
                testList.add("内容：123123123");
                testList.add("内容：123123123");
                testList.add("内容：123123123");
                testList.add("内容.ss");

                testList.add("内容：ss");
                testList.add("内容：ss");
//                LinkedHashMap<Integer, Bitmap> map= new LinkedHashMap();
//                map.put(0, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//                map.put(10, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//                map.put(12, BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
                //mIPrinterControl.printTest(testList);
                mIPrinterControl.printTest(testList);
            }
        });
        findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIPrinterControl.connectPrinter();
                List<AttributeBean> data = new ArrayList<>();
                for (int i = 0; i < 6; i++) {
                    AttributeBean info = new AttributeBean();
                    info.setContext_type(0);
                    info.setFont_gray(i%3);
                    info.setFont_size(i%3);
                    info.setContext("测试打印"+i);
                    if(i == 0 || i == 12){
                        info.setContext("打印内容士大夫快速导航富士康受打印内容士大夫快速导航富士"+i);
                        info.setFont_line(false);
                        info.setFont_gray(0);
                        info.setFont_size(2);
                    }

//                    if (i == 1|| i == 13){
//                        info.setContext("100元");
//                        info.setFont_gray(2);
//                    }
//                    if(i == 3|| i == 15){
//                        info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//                        info.setContext_type(1);
//                        info.setFont_gray(1);
//                        info.setFont_line(false);
//                    }
//
//                    if(i == 4|| i == 16){
//                        info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//                        info.setContext_type(1);
//                        info.setFont_gray(1);
//                        info.setFont_line(false);
//                    }
//
//                    if(i == 5|| i == 17){
//                        info.setBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.qrcode));
//                        info.setContext_type(1);
//                        info.setFont_gray(2);
//                    }
//
//                    if(i == 8|| i == 20){
//                        info.setContext_type(2);
//                    }
                   data.add(info);
               }
//
//                data.add(new AttributeBean(""));
//                data.add(new AttributeBean(""));
//                data.add(new AttributeBean(""));
//                data.add(new AttributeBean(""));

                mIPrinterControl.printString(data);
                mIPrinterControl.printString(data);
//                PrintContentToBitmap bt = new PrintContentToBitmap(data,80);
//                try {
//                    Bitmap bitmap = bt.Print();
//                    data.clear();
//                    data.add(new AttributeBean(0,1,"",1,bitmap));
//                    image.setImageBitmap(bitmap);
//                    mIPrinterControl.printString(data);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        });

        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIPrinterControl.openMoneyBox();
            }
        });

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIPrinterControl.disConnectPrinter();
            }
        });
    }

    /**
     * 返回精度
     * @param value 后台参数值
     * @return 精度
     */
    private static double precision(String value){
        double pre = 0.0;
        switch (value){
            case "0":
                pre = 1.0;
                break;
            case "1":
                pre = 0.1;
                break;
            case "2":
                pre = 0.01;
                break;
            case "3":
                pre = 0.001;
                break;
            case "4":
                pre = 0.0001;
                break;
        }
        return pre;
    }


}
