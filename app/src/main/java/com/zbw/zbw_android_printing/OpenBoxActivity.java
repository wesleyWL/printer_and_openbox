package com.zbw.zbw_android_printing;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.printConnector.NewPrint;

/**
 * 作者 ：Wesley
 * 时间 ：2023-12-25 10:32
 * 这个类是干嘛的？：OpenBoxActivity
 */
public class OpenBoxActivity extends Activity {
   private IPrinterControl mNewPrint;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_openbox);

      mNewPrint = NewPrint.getInstance().NewControl(NewPrint.PRINT_JD_K5, OpenBoxActivity.this, new IPrintCallBack() {
         @Override
         public void onConnect(String message, boolean b) {

         }

         @Override
         public void onDisConnect(String message, boolean b) {

         }

         @Override
         public void onPrintResult(boolean resultCode) {

         }
      });


      findViewById(R.id.openBoxTx).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            mNewPrint.openMoneyBox();
         }
      });


   }
}
