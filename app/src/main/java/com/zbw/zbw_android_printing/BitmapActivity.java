package com.zbw.zbw_android_printing;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.util.PrintContentToBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2022-10-31 15:25
 * BitmapActivity 说明：
 */
public class BitmapActivity extends Activity {
    private ImageView mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitmap);
        mView = findViewById(R.id.image);

        List<AttributeBean> data = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            AttributeBean info = new AttributeBean();
            info.setContext_type(0);
            info.setFont_gray(i%3);
            info.setFont_size(1);
            info.setContext("测试打印"+i);
            if(i == 0){
                info.setContext("打印内容士大夫快速导航富士康受打印内容士大夫快速导航"+i);
                //info.setFont_line(false);
                info.setFont_gray(0);
                info.setFont_size(1);
            }

            if (i == 1){
                info.setContext("*10大杯");
                info.setFont_gray(2);
            }
            if(i == 2){
                info.setContext_type(2);
            }

            data.add(info);
        }

        PrintContentToBitmap bt = new PrintContentToBitmap(data,80);
        try {
            Bitmap bitmap = bt.Print();
            Log.e("图片大小", String.valueOf(bitmap.getAllocationByteCount()));
            mView.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
