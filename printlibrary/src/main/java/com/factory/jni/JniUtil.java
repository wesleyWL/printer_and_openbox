package com.factory.jni;

public class JniUtil {
    static {
        System.loadLibrary("k5x");
    }

    public native int openCashBox();
    public native String serialPortRead(int devId);
    public native int serialPortClose(int devId);
    public native int serialPortOpen(String devName);
    public native int serialPortWrite(int devId,String data);
}
