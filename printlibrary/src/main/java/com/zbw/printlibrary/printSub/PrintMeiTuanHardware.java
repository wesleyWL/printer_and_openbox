package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.sankuai.hardware.mthwsrvmgrsdk.IPrintCallback;
import com.sankuai.hardware.mthwsrvmgrsdk.MTHardwareCenter;
import com.sankuai.mtsdk.MTSDKHelper;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.util.BitmapUtil;
import com.zbw.printlibrary.util.PrintThreadPoolManager;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者 ：Wesley
 * 时间 ：2023-02-10 11:25
 * PrintMeiTuanHardware 说明：
 */
public class PrintMeiTuanHardware implements IPrinterControl {
    private Context mContext;
    private final IPrintCallBack mCallBack;

    public PrintMeiTuanHardware(Context context, IPrintCallBack callBack) {
        mContext = context;
        mCallBack = callBack;
    }

    @Override
    public void connectPrinter() {
        MTSDKHelper.Init();
        MTHardwareCenter.get().init(mContext, new MTHardwareCenter.OnHSInitCallback() {
            @Override
            public void OnHSInitResult(boolean result) {
                Log.d("初始化结果", result ? "成功" : "失败");
                mCallBack.onConnect(result ? "打印机初始化成功" : "打印机初始化失败",result);
            }
        });
    }

    @Override
    public void disConnectPrinter() {

    }

    /**
     * 初始化
     */
    private void printInit() {
        byte[] initcmd = {0x1B, 0x40};//初始化
        MTHardwareCenter.get().print(initcmd, callback);
    }

    /**
     * 打印位置设置
     * 0 居左
     * 1 居中
     * 2 居右
     */
    private void printAlign(int align) {
        byte type;
        if (align == 0) {
            type = 0x00;
        } else if (align == 1) {
            type = 0x01;
        } else {
            type = 0x02;
        }
        byte[] initcmd1 = {0x1B, 0x61, type};//打印位置
        MTHardwareCenter.get().print(initcmd1, callback);
    }

    /**
     * 打印字体大小
     * 0 小字体
     * 1 中字体
     * 2 大字体
     */
    private void printSize(int size) {
        byte type;
        if (size == 0) {
            type = 0x00;
        } else if (size == 1) {
            type = 0x11;
        } else {
            type = 0x22;
        }
        byte[] initcmd1 = {0x1D, 0x21, type};//打印位置
        MTHardwareCenter.get().print(initcmd1, callback);
    }

    @Override
    public void printTest(final List<String> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    printInit();
                    for (int i = 0; i < testList.size(); i++) {
                        if (i == 0) {
                            printAlign(1);//打印位置
                            printSize(2);//打印大小
                            MTHardwareCenter.get().print(testList.get(0).getBytes("GBK"), callback);
                        } else if (i == 1) {
                            printAlign(1);//打印位置
                            printSize(1);//打印大小
                            MTHardwareCenter.get().print(testList.get(1).getBytes("GBK"), callback);
                        } else {
                            printAlign(0);//打印位置
                            printSize(0);//打印大小
                            MTHardwareCenter.get().print(testList.get(i).getBytes("GBK"), callback);
                        }
                        byte[] n = {0x0A};
                        MTHardwareCenter.get().print(n, callback);
                    }
                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    MTHardwareCenter.get().print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    private IPrintCallback callback = new IPrintCallback() {

        @Override
        public IBinder asBinder() {
            return null;
        }

        @Override
        public void onSuccess() throws RemoteException {

        }

        @Override
        public void onFailure(int i, String s) throws RemoteException {

        }
    };

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                printInit();
                try {
                    for (int i = 0; i < testList.size(); i++) {
                        AttributeBean info = testList.get(i);
                        printAlign(info.getFont_gray());
                        printSize(info.getFont_size());
                        if(info.getContext_type() == 1){
                            MTHardwareCenter.get().print(BitmapUtil.genBitmapCode(info.getBitmap()), callback);
                        }else{
                            MTHardwareCenter.get().print(info.getContext().getBytes("GBK"), callback);
                        }
                        if(info.isFont_line()){
                            byte[] n = {0x0A};
                            MTHardwareCenter.get().print(n, callback);
                        }
                    }
                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    MTHardwareCenter.get().print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {
        printString(testList);
    }

    @Override
    public void printStringAndBitmap(final List<String> testList, final LinkedHashMap<Integer, Bitmap> bitmapMap) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    printInit();
                    for (int i = 0; i < testList.size(); i++) {
                        if (i == 0) {
                            printAlign(1);//打印位置
                            printSize(2);//打印大小
                            MTHardwareCenter.get().print(testList.get(0).getBytes("GBK"), callback);
                        } else if (i == 1) {
                            printAlign(1);//打印位置
                            printSize(1);//打印大小
                            MTHardwareCenter.get().print(testList.get(1).getBytes("GBK"), callback);
                        } else {
                            printAlign(0);//打印位置
                            printSize(0);//打印大小
                            MTHardwareCenter.get().print(testList.get(i).getBytes("GBK"), callback);
                        }
                        byte[] n = {0x0A};
                        MTHardwareCenter.get().print(n, callback);

                        Bitmap bitmap = bitmapMap.get(i);
                        if (bitmap != null) {
                            //当前位置与需要打印位置一致 打印bitmap
                            MTHardwareCenter.get().print(BitmapUtil.genBitmapCode(bitmap), callback);
                            byte[] n1 = {0x0A};
                            MTHardwareCenter.get().print(n1, callback);
                        }
                    }

                    if (bitmapMap.size() > 0) {
                        //传入的position位置大于list的话默认在最后面打印
                        for (Map.Entry<Integer, Bitmap> entry : bitmapMap.entrySet()) {
                            if (entry.getKey() > testList.size() - 1) {
                                Bitmap showBitmap = entry.getValue();
                                MTHardwareCenter.get().print(BitmapUtil.genBitmapCode(showBitmap), callback);
                            }
                        }
                    }

                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    MTHardwareCenter.get().print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        MTSDKHelper.openCashbox();
    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
