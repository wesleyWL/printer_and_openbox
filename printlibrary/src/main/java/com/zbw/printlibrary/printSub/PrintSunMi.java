package com.zbw.printlibrary.printSub;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * Created by cyj on 2019-07-25.
 * 商米设备打印
 */

public class PrintSunMi extends PrinterControlCommon {
    // 商米打印
    private IWoyouService woyouService;
    private Context mContext;
    private final IPrintCallBack mCallBack;
    private final boolean isConnected = false;
    private Intent intent;

    public PrintSunMi(@NonNull Context context, IPrintCallBack callback) {
        mContext = context;
        this.mCallBack = callback;
    }

    @Override
    public void connectPrinter() {
        if (woyouService == null) {
            intent = new Intent();
            intent.setPackage("woyou.aidlservice.jiuiv5");
            intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
            boolean flag = mContext.bindService(intent, connService, Context.BIND_AUTO_CREATE);
            Log.d("tag", "connectPrinter: " + flag);
        }

    }

    @Override
    public void disConnectPrinter() {
        /*Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");*/
//        mContext.stopService(intent);
        if (woyouService != null) {
            mContext.unbindService(connService);
            woyouService = null;
        }
        if (mContext != null) {
            mContext = null;
        }
        mCallBack.onDisConnect("断开成功", true);
    }

    @Override
    public void printTest(final List<String> liststr) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    woyouService.setAlignment(1, callback);
                    woyouService.printTextWithFont(liststr.get(0) + "\n", "", 48, callback);
                    woyouService.printTextWithFont(liststr.get(1) + "\n", "", 36, callback);
                    woyouService.setAlignment(0, callback);
                    for (int i = 2; i < liststr.size(); i++) {
                        woyouService.printTextWithFont(liststr.get(i) + "\n", "", 24, callback);
                    }
                    woyouService.lineWrap(1, callback);
                    //切纸
                    woyouService.cutPaper(callback);
                    mCallBack.onPrintResult(true);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < testList.size(); i++) {
                        float fontSize = 0;
                        String line = "";
                        switch (testList.get(i).getFont_size()) {
                            case 0:
                                fontSize = 24;
                                break;
                            case 1:
                                fontSize = 36;
                                break;
                            case 2:
                                fontSize = 48;
                                break;
                        }
                        if (testList.get(i).isFont_line()) {
                            line = "\n";
                        }

                        woyouService.setAlignment(testList.get(i).getFont_gray(), callback);
                        if (1 == testList.get(i).getContext_type()) {
                            woyouService.printBitmap(testList.get(i).getBitmap(), callback);
                            //bitmap未换行 输出换行符
                            woyouService.printText("\n", callback);
                        } else {
                            woyouService.printTextWithFont(testList.get(i).getContext() + line, "", fontSize, callback);
                        }
                    }
                    woyouService.lineWrap(1, callback);
                    //切纸
                    woyouService.cutPaper(callback);
                    mCallBack.onPrintResult(true);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });

    }

    @Override
    public void printStringAndBitmap(final List<String> liststr, final LinkedHashMap<Integer, Bitmap> bitmapHashMap) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < liststr.size(); i++) {
                        String msg = liststr.get(i);
                        switch (i) {
                            case 0:
                                woyouService.setAlignment(1, callback);
                                woyouService.printTextWithFont(msg + "\n", "", 48, callback);
                                break;
                            case 1:
                                woyouService.setAlignment(1, callback);
                                woyouService.printTextWithFont(msg + "\n", "", 36, callback);
                                break;
                            default:
                                if (msg.startsWith("牌号")) {
                                    woyouService.setAlignment(0, callback);
                                    woyouService.printTextWithFont(msg + "\n", "", 36, callback);
                                } else {
                                    woyouService.setAlignment(0, callback);
                                    woyouService.printTextWithFont(msg + "\n", "", 24, callback);
                                }
                                break;
                        }
                        Bitmap bitmap = bitmapHashMap.get(i);
                        if (bitmap != null) {
                            //当前位置与需要打印位置一致 打印bitmap
                            woyouService.setAlignment(1, callback);
                            woyouService.printBitmap(bitmap, callback);
                            //bitmap未换行 输出换行符
                            woyouService.printText("\n", callback);
                        }
                    }
                    if (bitmapHashMap.size() > 0) {
                        woyouService.setAlignment(1, null);
                        //传入的position位置大于list的话默认在最后面打印
                        for (Map.Entry<Integer, Bitmap> entry : bitmapHashMap.entrySet()) {
                            if (entry.getKey() > liststr.size() - 1) {
                                Bitmap showBitmap = entry.getValue();
                                woyouService.printBitmap(showBitmap, null);
                            }
                        }
                    }
                    woyouService.lineWrap(1, callback);
                    //切纸
                    woyouService.cutPaper(callback);
                    mCallBack.onPrintResult(true);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });

    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }


    @Override
    public void openMoneyBox() {
        //开钱箱
        try {
            if (woyouService != null) {
                woyouService.openDrawer(callback);
            }
        } catch (RemoteException e) {
            Toast.makeText(mContext, "没有找到商米模块", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean isConnected() {
        return woyouService == null;
    }

    public ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
//            mCallBack.onDisConnect("断开成功",true);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
            mCallBack.onConnect("连接成功", true);
        }
    };


    private final ICallback callback = new ICallback() {

        @Override
        public IBinder asBinder() {
            return null;
        }

        @Override
        public void onRunResult(boolean isSuccess, int code, String msg) throws RemoteException {
            if (isSuccess) {
//                mCallBack.onPrintResult(PRINT_SUCCESS);
            } else {
//                mCallBack.onPrintResult(PRINT_FAIL);
            }
        }
    };

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
