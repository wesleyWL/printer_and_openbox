package com.zbw.printlibrary.printSub;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.meituan.printerservice2.PrintCallback;
import com.meituan.printerservice2.PrinterManager;
import com.sankuai.mtsdk.MTSDKHelper;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.util.BitmapUtil;
import com.zbw.printlibrary.util.PrintThreadPoolManager;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者 ：Wesley
 * 时间 ：2023-02-09 17:21
 * PrintMeiTuanS4Sp 说明：
 * 美团一体收银机
 */
public class PrintMeiTuanS4Sp implements IPrinterControl {
    private Context mContext;
    private final IPrintCallBack mCallBack;
    public PrinterManager manager = null;

    public PrintMeiTuanS4Sp(Context context, IPrintCallBack callBack) {
        mContext = context;
        mCallBack = callBack;
    }

    @Override
    public void connectPrinter() {
        MTSDKHelper.Init();
        if(manager == null){
            Log.d("初始化：","connectPrinter");
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.meituan.printerservice2", "com.sankuai.hardware.PrinterService"));
            mContext.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("初始化：","connectPrinter_成功");
            manager = PrinterManager.Stub.asInterface(service);
        }
    };

    @Override
    public void disConnectPrinter() {
        if (manager != null) {
            mContext.unbindService(conn);
            manager = null;
        }
        if (mContext != null) {
            mContext = null;
        }
        mCallBack.onDisConnect("断开成功", true);
    }

    private PrintCallback callback = new PrintCallback.Stub() {
        @Override
        public void onSuccess() throws RemoteException {}

        @Override
        public void onFailure(int code, String msg) throws RemoteException {
            Log.d("打印失败",msg);
        }
    };

    /**
     * 初始化
     */
    private void printInit() {
        byte[] initcmd = {0x1B, 0x40};//初始化
        try {
            if(manager != null) {
                manager.print(initcmd, callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印位置设置
     * 0 居左
     * 1 居中
     * 2 居右
     */
    private void printAlign(int align) {
        byte type;
        if (align == 0) {
            type = 0x00;
        } else if (align == 1) {
            type = 0x01;
        } else {
            type = 0x02;
        }
        byte[] initcmd1 = {0x1B, 0x61, type};//打印位置
        try {
            if(manager != null) {
                manager.print(initcmd1, callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印字体大小
     * 0 小字体
     * 1 中字体
     * 2 大字体
     */
    private void printSize(int size) {
        byte type;
        if (size == 0) {
            type = 0x00;
        } else if (size == 1) {
            type = 0x11;
        } else {
            type = 0x22;
        }
        byte[] initcmd1 = {0x1D, 0x21, type};//打印位置
        try {
            if(manager != null) {
                manager.print(initcmd1, callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printTest(final List<String> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    printInit();
                    if(manager == null){
                        mCallBack.onPrintResult(false);
                        return;
                    }

                    for (int i = 0; i < testList.size(); i++) {
                        if (i == 0) {
                            printAlign(1);//打印位置
                            printSize(2);//打印大小
                            manager.print(testList.get(0).getBytes("GBK"), callback);
                        } else if (i == 1) {
                            printAlign(1);//打印位置
                            printSize(1);//打印大小
                            manager.print(testList.get(1).getBytes("GBK"), callback);
                        } else {
                            printAlign(0);//打印位置
                            printSize(0);//打印大小
                            manager.print(testList.get(i).getBytes("GBK"), callback);
                        }
                        byte[] n = {0x0A};
                        manager.print(n, callback);
                    }
                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    manager.print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                printInit();
                if(manager == null){
                    mCallBack.onPrintResult(false);
                    return;
                }
                try {
                    for (int i = 0; i < testList.size(); i++) {
                        AttributeBean info = testList.get(i);
                        printAlign(info.getFont_gray());
                        printSize(info.getFont_size());
                        if(info.getContext_type() == 1){
                            manager.print(BitmapUtil.genBitmapCode(info.getBitmap()), callback);
                        }else{
                            manager.print(info.getContext().getBytes("GBK"), callback);
                        }
                        if(info.isFont_line()){
                            byte[] n = {0x0A};
                            manager.print(n, callback);
                        }
                    }
                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    manager.print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {
        printString(testList);
    }

    @Override
    public void printStringAndBitmap(final List<String> testList, final LinkedHashMap<Integer, Bitmap> bitmapMap) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    printInit();
                    if(manager == null){
                        mCallBack.onPrintResult(false);
                        return;
                    }
                    for (int i = 0; i < testList.size(); i++) {
                        if (i == 0) {
                            printAlign(1);//打印位置
                            printSize(2);//打印大小
                            manager.print(testList.get(0).getBytes("GBK"), callback);
                        } else if (i == 1) {
                            printAlign(1);//打印位置
                            printSize(1);//打印大小
                            manager.print(testList.get(1).getBytes("GBK"), callback);
                        } else {
                            printAlign(0);//打印位置
                            printSize(0);//打印大小
                            manager.print(testList.get(i).getBytes("GBK"), callback);
                        }
                        byte[] n = {0x0A};
                        manager.print(n, callback);

                        Bitmap bitmap = bitmapMap.get(i);
                        if (bitmap != null) {
                            //当前位置与需要打印位置一致 打印bitmap
                            manager.print(BitmapUtil.genBitmapCode(bitmap), callback);
                            byte[] n1 = {0x0A};
                            manager.print(n1, callback);
                        }
                    }

                    if (bitmapMap.size() > 0) {
                        //传入的position位置大于list的话默认在最后面打印
                        for (Map.Entry<Integer, Bitmap> entry : bitmapMap.entrySet()) {
                            if (entry.getKey() > testList.size() - 1) {
                                Bitmap showBitmap = entry.getValue();
                                manager.print(BitmapUtil.genBitmapCode(showBitmap), callback);
                            }
                        }
                    }

                    byte[] cut = {0x0A, 0x0A, 0x0A, 0x0A, 0x1D, 0x56, 0x01};
                    manager.print(cut, callback);
                } catch (Exception e) {
                    mCallBack.onPrintResult(false);
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        MTSDKHelper.openCashbox();
    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
