package com.zbw.printlibrary.printSub;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.util.Log;
import com.hisense.hs650service.aidl.HS650Api;
import com.hisense.hs650service.aidl.LedStripe;
import com.hisense.hs650service.aidl.Printer;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者 ：Wesley
 * 时间 ：2023-06-20 16:05
 * 这个类是干嘛的？：PrintHisense
 */
public class PrintHisense implements IPrinterControl {
   private static Printer p = null;
   private HS650Api hs650ApiHandler = null;
   private LedStripe ledStripe = null;
   private Context mContext;
   private final IPrintCallBack mCallBack;

   public PrintHisense(Context context, IPrintCallBack callBack) {
      mContext = context;
      mCallBack = callBack;
   }

   @Override
   public void connectPrinter() {
      if(hs650ApiHandler == null){
         Intent intent = new Intent();
         intent.setPackage("com.hisense.hs650api");
         mContext.bindService(intent, conn, Service.BIND_AUTO_CREATE);
      }
   }

   private ServiceConnection conn = new ServiceConnection() {
      @Override
      public void onServiceConnected(ComponentName name, IBinder service) {
         // TODO Auto-generated method stub
         hs650ApiHandler = HS650Api.Stub.asInterface(service);
         if (hs650ApiHandler == null) {
            Log.d("PrintHisense","启动服务失败");
            mCallBack.onConnect("启动服务失败",false);
         }else {
            try {
               p = hs650ApiHandler.getPrinter();
               p.printerConfig(115200,0,true);
               p.initPrinter(1);
               ledStripe = hs650ApiHandler.getLedStripe();
               mCallBack.onConnect("连接打印机成功",true);
            } catch (Exception e) {
               e.printStackTrace();
               mCallBack.onConnect("启动服务失败",false);
            }
         }
      }

      @Override
      public void onServiceDisconnected(ComponentName name) {
         hs650ApiHandler = null;
      }
   };

   @Override
   public void disConnectPrinter() {
      if(p != null){
         try {
            p.closePrinter();
            if (hs650ApiHandler != null) {
               mContext.unbindService(conn);
               hs650ApiHandler = null;
            }
            if (mContext != null) {
               mContext = null;
            }
            mCallBack.onDisConnect("断开成功", true);
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   @Override
   public void printTest(List<String> testList) {
      if(p != null){
         try {
            openLED(true);
            p.printTextStr(testList.get(0)+"\n",1,1);
            p.printTextStr(testList.get(1)+"\n",1,1);
            for (int i = 2; i < testList.size(); i++) {
               p.printTextStr(testList.get(i)+"\n",0,0);
            }
            p.feedPaper(2, 0);
            p.cutPaper();
            openLED(false);
         }catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   @Override
   public void printString(List<AttributeBean> testList) {
         if(p != null){
            try {
               openLED(true);
               for (int i = 0; i < testList.size(); i++) {
                  AttributeBean bean = testList.get(i);
                  int align = bean.getFont_gray();
                  int size = bean.getFont_size()==0?0:1;
                  if(bean.getContext_type() == 1){//图片
                     p.printImage(bean.getBitmap(),align);
                  }else {
                     if(bean.isFont_line()){
                        p.printTextStr(bean.getContext()+"\n",size,align);
                     }else {
                        p.printTextStr(bean.getContext(),size,align);
                     }
                  }
               }
               p.feedPaper(2, 0);
               p.cutPaper();
               openLED(false);
            }catch (Exception e) {
               e.printStackTrace();
            }
         }
   }

   @Override
   public void printString(List<AttributeBean> testList, int width) {
      printString(testList);
   }

   @Override
   public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {
      if(p != null){
         try {
            openLED(true);
            for (int i = 0; i < testList.size(); i++) {
               String msg = testList.get(i)+"\n";
               switch (i) {
                  case 0:
                  case 1:
                     p.printTextStr(msg,1,1);
                     break;
                  default:
                     if (msg.startsWith("牌号")) {
                        p.printTextStr(msg,1,1);
                     } else {
                        p.printTextStr(msg,0,0);
                     }
                     break;
               }
               Bitmap bitmap = bitmapMap.get(i);
               if (bitmap != null) {
                  //当前位置与需要打印位置一致 打印bitmap
                  p.printImage(bitmap,1);
               }
            }
            if (bitmapMap.size() > 0) {
               //传入的position位置大于list的话默认在最后面打印
               for (Map.Entry<Integer, Bitmap> entry : bitmapMap.entrySet()) {
                  if (entry.getKey() > testList.size() - 1) {
                     Bitmap showBitmap = entry.getValue();
                     p.printImage(showBitmap,1);
                  }
               }
            }
            p.feedPaper(2, 0);
            p.cutPaper();
            openLED(false);
         }catch (Exception e){
            e.printStackTrace();
         }
      }
   }

   private void openLED(boolean isOpen){
      try {
         if(ledStripe != null) {
            if (isOpen) {
               ledStripe.lightOn();
            } else {
               ledStripe.lightOff();
            }
         }
      }catch (Exception e){
         e.printStackTrace();
      }
   }

   @Override
   public void blueConnect(String address) {

   }

   @Override
   public void disBlueConnect() {

   }

   @Override
   public void openMoneyBox() {

   }

   @Override
   public boolean isConnected() {
      return false;
   }
}
