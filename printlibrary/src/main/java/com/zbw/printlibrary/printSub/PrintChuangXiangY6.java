package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.szsicod.print.escpos.PrinterAPI;
import com.szsicod.print.io.InterfaceAPI;
import com.szsicod.print.io.USBAPI;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.CheckUtil;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 创享-Y6设备打印
 */
public class PrintChuangXiangY6 extends PrinterControlCommon {
    private Context mContext;
    private IPrintCallBack mCallBack;
    private PrinterAPI mPrinter;

    public PrintChuangXiangY6(@NonNull Context mContext, @NonNull IPrintCallBack callback) {
        this.mContext = mContext;
        this.mCallBack = callback;
        mPrinter = PrinterAPI.getInstance();
    }

    @Override
    public void connectPrinter() {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    InterfaceAPI io = new USBAPI(mContext);
                    int resultCode = mPrinter.connect(io);
                    if (PrinterAPI.SUCCESS == resultCode) {
                        mCallBack.onConnect("连接成功", true);
                    } else {
                        mCallBack.onConnect("连接失败", false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void disConnectPrinter() {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    mPrinter.disconnect();
                    mCallBack.onDisConnect("断开成功", true);
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallBack.onDisConnect("断开失败", false);
                }
            }
        });
    }

    @Override
    public void printTest(final List<String> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    doPrintString(testList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    doPrint(testList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return mPrinter.isConnect();
    }

    private void doPrint(List<AttributeBean> textList) {
        if (CheckUtil.isEmpty(textList)) {
            return;
        }
        try {

            for (int i = 0; i < textList.size(); i++) {
                // 小票主要内容
                mPrinter.setCharSize(textList.get(i).getFont_size(), textList.get(i).getFont_size());
                mPrinter.setAlignMode(textList.get(i).getFont_gray());
                mPrinter.printString(textList.get(i).getContext(), "GBK", true);
            }
            printLine(mPrinter, 1);
            // 走纸4行,再切纸
            mPrinter.cutPaper(66, 0);

            mCallBack.onPrintResult(true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onPrintResult(false);
        }
    }

    private void doPrintString(List<String> textList) {
        if (CheckUtil.isEmpty(textList)) {
            return;
        }
        try {
            String title = textList.get(0);
            mPrinter.setCharSize(1, 1);
            mPrinter.setAlignMode(1);
            mPrinter.printString(title, "GBK", true);
            mPrinter.setCharSize(0, 0);
            mPrinter.setAlignMode(0);
            for (int i = 1; i < textList.size(); i++) {
                // 小票主要内容
                mPrinter.printString(textList.get(i), "GBK", true);
            }
            printLine(mPrinter, 1);
            // 走纸4行,再切纸
            mPrinter.cutPaper(66, 0);

            mCallBack.onPrintResult(true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onPrintResult(false);
        }
    }

    private void printLine(PrinterAPI printer, int line) {
        if (printer == null || line < 0) {
            return;
        }
        try {
            for (int i = 0; i < line; i++) {
                printer.printString("\n", "GBK", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
