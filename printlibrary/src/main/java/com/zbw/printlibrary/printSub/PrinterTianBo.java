package com.zbw.printlibrary.printSub;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;

import com.telpo.hprt.printerlibrary.HPRTPrinterUtil;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.CheckUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PrinterTianBo extends PrinterControlCommon {
    private final Context mContext;
    private final IPrintCallBack mCallBack;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_OK)) {
                mCallBack.onConnect("连接成功", true);
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_NOPAPER)) {
                printLog("打印机缺纸");
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_COVER_OPENED)) {
                printLog("打印机开盖");
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_OVER_HOT)) {
                printLog("打印机过热");
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_OFFLINE)) {
                printLog("打印机离线");
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_UNKNOWN)) {
                printLog("打印机未知错误");
            } else if (action.equals(HPRTPrinterUtil.ACTION_PRINTER_NOFOUND)) {
                printLog("打印机未找到");
            }

        }
    };
    private HPRTPrinterUtil printerUtil;


    public PrinterTianBo(@NonNull Context context, @NonNull IPrintCallBack callBack) {
        mContext = context;
        mCallBack = callBack;
    }

    @Override
    public void connectPrinter() {
        printerUtil = new HPRTPrinterUtil(mContext, new HPRTPrinterUtil.PrinterListener() {
            @Override
            public void onPrinterInitSuccess() {
                printLog("打印机连接成功");
            }

            @Override
            public void onPrinterDeath() {
                printLog("打印机断开");
            }
        });
        registerReceiver();
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_OK);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_NOPAPER);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_OVER_HOT);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_COVER_OPENED);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_OFFLINE);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_NOFOUND);
        filter.addAction(HPRTPrinterUtil.ACTION_PRINTER_UNKNOWN);
        mContext.registerReceiver(mReceiver, filter);
    }

    @Override
    public void disConnectPrinter() {
        try {
            printerUtil.release();
            mContext.unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
            printLog("断开天波打印机失败：" + e.getMessage());
        }
    }

    @Override
    public void printTest(List<String> testList) {
        printTextList(testList);
    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {
        printTest(testList);
        printBitmapMap(bitmapMap);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        printerUtil.OpenCashdrawer(0);
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    private void printBitmapMap(LinkedHashMap<Integer, Bitmap> bitmapMap) {
        if (bitmapMap == null || bitmapMap.size() == 0) {
            return;
        }
        try {
            printerUtil.Initialize();
            for (Map.Entry<Integer, Bitmap> entry : bitmapMap.entrySet()) {
                Bitmap bitmap = entry.getValue();
                try {
                    printerUtil.PrintBitmap(bitmap, (byte) 1, (byte) 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printTextList(List<String> textList) {
        if (CheckUtil.isEmpty(textList)) {
            return;
        }
        String title = textList.get(0);
        try {
            printerUtil.Initialize();
            // 居中
            printerUtil.SetJustification(1);
            // 大字体
            printerUtil.SelectCharacterFont((byte) 0);
            printerUtil.PrintText(title + "\r\n");

            // 居左
            printerUtil.SetJustification(0);
            // 小字体
            printerUtil.SelectCharacterFont((byte) 1);
            for (int i = 1; i < textList.size(); i++) {
                printerUtil.PrintText(textList.get(i) + "\r\n", 0, 0, 0);
            }

            printerUtil.Initialize();
            printerUtil.PrintAndFeedNLine((byte) 3);    // 走纸三行
//            printLine(3);
            //切纸 (半切)
            printerUtil.CutPaper(49);
            mCallBack.onPrintResult(true);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this.getClass().getSimpleName(), "打印失败：" + e.getMessage());
            mCallBack.onPrintResult(false);
        }
    }

    /**
     * 打印换行
     */
    private void printLine(int nums) throws Exception {
        if (nums <= 0) return;
        for (int i = 0; i < nums; i++) {
            printerUtil.PrintText("\r\n");
        }
    }

    private void printLog(String message) {
        Log.d(this.getClass().getSimpleName(), message);
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
