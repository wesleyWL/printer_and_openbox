package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.weleadin.sdk.cashbox.CashBox;
import com.weleadin.sdk.printer.Printer;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by cyj on 2019-07-25.
 * 微领地打印
 */

public class PrintWld extends PrinterControlCommon {

    private Context mContext;
    private IPrintCallBack mCallBack;
    private Printer printer;
    private CashBox cashBox;

    public PrintWld(@NonNull Context context, IPrintCallBack callback) {
        mContext = context;
        this.mCallBack = callback;
    }

    @Override
    public void connectPrinter() {
        try {
            printer = new Printer(mContext);
            cashBox = new CashBox(mContext);
            printer.connectPrinter();
            mCallBack.onConnect("连接成功", true);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            mCallBack.onConnect("连接失败", false);
        }

    }

    @Override
    public void disConnectPrinter() {
        if (printer != null) {
            printer.disconnectPrinter();
            printer = null;
            mCallBack.onDisConnect("断开成功", true);
        }
        if (mContext != null) {
            mContext = null;
        }
    }

    @Override
    public void printTest(final List<String> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {

                    if (printer.hasPaper()) {
                        String Str1 = testList.get(0);
                        printer.setFontSize(2);
                        printer.printString(headCenter(Str1, 16));

                        String Str2 = testList.get(1);
                        printer.setFontSize(2);
                        printer.printString(headCenter(Str2, 16));

                        for (int i = 2; i < testList.size(); i++) {
                            printer.setFontSize(1);
                            printer.printString(testList.get(i));
                        }
                        printer.moveBlankLine(6);
                        mCallBack.onPrintResult(true);
                    } else {
                        Toast.makeText(mContext, "缺少打印纸", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    if (printer.hasPaper()) {
                        for (int i = 2; i < testList.size(); i++) {
                            if (testList.get(i).getFont_size() == 0) {
                                printer.setFontSize(1);
                            } else {
                                printer.setFontSize(2);
                            }
                            if (testList.get(i).getFont_gray() == 1) {
                                printer.printString(headCenter(testList.get(i).getContext(), 16));
                            } else {
                                printer.printString(testList.get(i).getContext());
                            }
                        }
                        printer.moveBlankLine(6);
                        mCallBack.onPrintResult(true);
                    } else {
                        Toast.makeText(mContext, "缺少打印纸", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    /**
     * 居中显示
     *
     * @param headStr 需要居中的字符串
     * @param Length  所占空间的长度（一般为小票所占单字节长度）
     * @return 输出标题
     */
    private static String headCenter(String headStr, int Length) {
        if (headStr == null)
            headStr = " ";
        StringBuilder srr = new StringBuilder();
        int Num;
        int stt = 0;
        char[] chars = headStr.toCharArray();
        for (char aChar : chars) {
            byte[] bytes = ("" + aChar).getBytes();
            if (bytes.length == 1) {
                Num = 1;
            } else {
                Num = 2;
            }
            stt += Num;
        }

        if (Length > stt) {
            for (int i = 0; i < (Length - stt) / 2; i++)
                srr.append(" ");
            srr.append(headStr);
        } else {
            srr = new StringBuilder(headStr);
        }
        return srr.toString();
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }


    @Override
    public void openMoneyBox() {
        if (cashBox != null) {
            cashBox.openCashBox();
        }

    }

    @Override
    public boolean isConnected() {
        if (printer == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}