package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;


import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.aclas.ComBean;
import com.zbw.printlibrary.aclas.SerialHelper;
import com.zbw.printlibrary.util.SerialPortFinder;
import com.zbw.printlibrary.aclas.StringUtils;

import net.posprinter.utils.DataForSendToPrinterPos80;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * 作者：cyj
 * 时间：2020-05-18 17:24
 * 注明：顶尖Aclas一体称
 */
public class PrintAclas extends PrinterControlCommon {
    private Context mContext;
    private IPrintCallBack mCallBack;
    private static final String TAG = "PrintAclas";
    private SerialPortFinder serialPortFinder;
    private SerialHelper serialHelper;

    public PrintAclas(Context mContext, IPrintCallBack mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
    }

    @Override
    public void connectPrinter() {
        serialHelper = new SerialHelper() {
            @Override
            protected void onDataReceived(final ComBean comBean) {
            }
        };
        serialHelper.setBaudRate(9600);
        serialHelper.setPort("/dev/ttyS3");
        try {
            serialHelper.open();
            mCallBack.onConnect("连接成功", true);
        } catch (IOException e) {
            e.printStackTrace();
            mCallBack.onConnect("连接失败", false);
        }
    }

    @Override
    public void disConnectPrinter() {
        try {
            serialHelper.close();//关闭串口
            mCallBack.onDisConnect("断开成功", true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onDisConnect("断开失败", false);
        }
    }

    @Override
    public void printTest(List<String> testList) {
        try {
            if (testList != null && testList.size() > 0) {
                for (int i = 0; i < testList.size(); i++) {
                    if (0 == i || 1 == i) {
                        serailPrint(TextData(testList.get(i), true));
                    } else {
                        serailPrint(TextData(testList.get(i), false));
                    }
                }
            }
            mCallBack.onPrintResult(true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onPrintResult(false);
        }

    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    /**
     * 数据发给串口
     */
    private void serailPrint(List<byte[]> data) {

        if (data.size() != 0 && serialHelper != null) {
            for (int i = 0; i < data.size(); i++) {
                serialHelper.send(data.get(i));
            }
        } else {
            Toast.makeText(mContext, "数据或串口助手为空", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 文本
     */
    private List<byte[]> TextData(String str, boolean isCenter) {
        List<byte[]> list = new ArrayList<>();
        list.add(DataForSendToPrinterPos80.initializePrinter());
        if (isCenter) {
            list.add(DataForSendToPrinterPos80.selectAlignment(1));//居中
        } else {
            list.add(DataForSendToPrinterPos80.selectAlignment(10));//靠左
        }
        list.add(StringUtils.strTobytes(str));
        list.add(DataForSendToPrinterPos80.printAndFeedLine());
        return list;
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
