package com.zbw.printlibrary.printSub;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.meituan.pos.holygrail.sdk.IDeviceService;
import com.meituan.pos.holygrail.sdk.printer.AlignType;
import com.meituan.pos.holygrail.sdk.printer.FontScale;
import com.meituan.pos.holygrail.sdk.printer.FontType;
import com.meituan.pos.holygrail.sdk.printer.IPrinter;
import com.meituan.pos.holygrail.sdk.printer.PrintCallback;
import com.meituan.pos.holygrail.sdk.printer.PrinterStatus;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者 ：Wesley
 * 时间 ：2023-02-09 10:33
 * PrintMeiTuanM1 说明：
 * 美团手持机
 */
public class PrintMeiTuanM1 implements IPrinterControl {
    private Context mContext;
    private final IPrintCallBack mCallBack;
    public IPrinter printDev = null;
    public static IDeviceService deviceService = null;
    public PrintMeiTuanM1(Context context, IPrintCallBack callBack) {
        mContext = context;
        mCallBack = callBack;
    }

    public ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("PrintMeiTuanM1", "服务已断开");
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("PrintMeiTuanM1", "服务已连接");
            deviceService = IDeviceService.Stub.asInterface(service);
            if (null != deviceService) {
                onDeviceConnected(deviceService);
            }
        }
    };

    public void onDeviceConnected(IDeviceService deviceService) {
        try {
            printDev = IPrinter.Stub.asInterface(deviceService.getPrinter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectPrinter() {
        if(printDev == null){
            Intent intent = new Intent();
            intent.setPackage("com.centerm.mtsdk");
            intent.setAction("meituan_pos_device_service");
            mContext.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void disConnectPrinter() {
        if (printDev != null) {
            mContext.unbindService(conn);
            printDev = null;
        }
        if (mContext != null) {
            mContext = null;
        }
        mCallBack.onDisConnect("断开成功", true);
    }

    @Override
    public void printTest(List<String> testList) {
        if(printDev != null){
            try {
                printDev.addAlignedText(2, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_2X2,testList.get(0));
                printDev.addAlignedText(2, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_2X2,testList.get(1));
                for (int i = 2; i < testList.size(); i++) {
                    printDev.addAlignedText(1, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_1X1,testList.get(i));
                }
                printDev.start(printCallback);
            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private PrintCallback printCallback = new PrintCallback.Stub() {
        @Override
        public void onFinish(int i) throws RemoteException {
            Log.d("printCallback","打印状态码：" + i);
            switch(i){
                case PrinterStatus.ERROR_NONE:
                    Log.d("printCallback","打印结束");
                    try {
                        printDev.close();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case PrinterStatus.ERROR_PAPER_ENDED:
                    Log.d("printCallback","打印机缺纸");
                    break;
                case PrinterStatus.ERROR_OVERHEAT:
                    Log.d("printCallback","打印机过热");
                    break;
                case PrinterStatus.ERROR_LOWVOL:
                    Log.d("printCallback","打印机低电量");
                    break;
                default:
                    Log.d("printCallback","打印机状态未知");
                    break;
            }
        }
    };

    @Override
    public void printString(List<AttributeBean> testList) {
        if(printDev != null){
            try {
                for (int i = 0; i < testList.size(); i++) {
                    AttributeBean info = testList.get(i);
                    int align = info.getFont_gray()+1;
                    int scale = 0;
                    if(info.getFont_size() == 0){
                        scale = FontScale.FONT_SCALE_WIDTH_HEIGHT_1X1;
                    }else if(info.getFont_size() == 1){
                        scale = FontScale.FONT_SCALE_WIDTH_HEIGHT_2X2;
                    }else if(info.getFont_size() == 2){
                        scale = FontScale.FONT_SCALE_WIDTH_HEIGHT_3X3;
                    }

                    if (1 == testList.get(i).getContext_type()) {
                        printDev.addAlignedImage(align, info.getBitmap());
                    } else {
                        printDev.addAlignedText(align, FontType.FONT_TYPE_2, scale,info.getContext());
                    }
                }
                printDev.start(printCallback);
            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {
        if (printDev != null) {
            try {
                for (int i = 0; i < testList.size(); i++) {
                    String msg = testList.get(i);
                    switch (i) {
                        case 0:
                        case 1:
                            printDev.addAlignedText(2, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_2X2, msg);
                            break;
                        default:
                            if (msg.startsWith("牌号")) {
                                printDev.addAlignedText(1, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_2X2, msg);
                            } else {
                                printDev.addAlignedText(1, FontType.FONT_TYPE_2, FontScale.FONT_SCALE_WIDTH_HEIGHT_1X1, msg);
                            }
                            break;
                    }
                    Bitmap bitmap = bitmapMap.get(i);
                    if (bitmap != null) {
                        //当前位置与需要打印位置一致 打印bitmap
                        printDev.addAlignedImage(2, bitmap);
                    }
                }
                if (bitmapMap.size() > 0) {
                    //传入的position位置大于list的话默认在最后面打印
                    for (Map.Entry<Integer, Bitmap> entry : bitmapMap.entrySet()) {
                        if (entry.getKey() > testList.size() - 1) {
                            Bitmap showBitmap = entry.getValue();
                            printDev.addAlignedImage(2, showBitmap);
                        }
                    }
                }
                printDev.start(printCallback);
            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
