package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.sun.jna.Pointer;
import com.sun.jna.WString;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.printerlibs.printerlibs_caysnpos.printerlibs_caysnpos;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 吉成-Q1设备打印
 */
public class PrintJiChengQ1 extends PrinterControlCommon {
    public static String USB_PORT = "";   //usb端口

    private Context mContext;
    private IPrintCallBack mCallBack;
    private Pointer mPointer = Pointer.NULL;

    public PrintJiChengQ1(@NonNull Context mContext, @NonNull IPrintCallBack mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
    }

    private printerlibs_caysnpos.on_port_closed_callback closed_callback = new printerlibs_caysnpos.on_port_closed_callback() {
        @Override
        public void on_port_closed(Pointer private_data) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    disConnectPrinter();
                }
            });
        }
    };

    @Override
    public void connectPrinter() {
        try {
            if (TextUtils.isEmpty(USB_PORT)) {
                mCallBack.onConnect("连接失败，必须设置USB端口---NewPrint.setUsbPort()", false);
                return;
            }
            mPointer = printerlibs_caysnpos.INSTANCE.CaysnPos_OpenUsbVidPidStringA(USB_PORT);
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetClosedEvent(mPointer, closed_callback, Pointer.NULL);
            mCallBack.onConnect("连接成功", true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onConnect("连接失败", false);
        }
    }

    @Override
    public void disConnectPrinter() {
        try {
            mPointer = printerlibs_caysnpos.INSTANCE.CaysnPos_OpenUsbVidPidStringA(USB_PORT);
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetClosedEvent(mPointer, closed_callback, Pointer.NULL);
            mCallBack.onDisConnect("断开成功", true);
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onDisConnect("断开失败", false);
        }
    }

    @Override
    public void printTest(List<String> testList) {
        try {
            if (mPointer == null) {
                mCallBack.onPrintResult(false);
                return;
            }
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetMultiByteMode(mPointer);
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetMultiByteEncoding(mPointer, printerlibs_caysnpos.MultiByteModeEncoding_UTF8);
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetAlignment(mPointer, printerlibs_caysnpos.PosAlignment_HCenter);
            String title = testList.get(0);
            printerlibs_caysnpos.INSTANCE.CaysnPos_PrintTextInUTF8W(mPointer, new WString(title + "\r\n"));
            printerlibs_caysnpos.INSTANCE.CaysnPos_SetAlignment(mPointer, printerlibs_caysnpos.PosAlignment_Left);
            for (int i = 1; i < testList.size(); i++) {
                printerlibs_caysnpos.INSTANCE.CaysnPos_PrintTextInUTF8W(mPointer, new WString(testList.get(i) + "\r\n"));
            }
            printLine(5);
            printerlibs_caysnpos.INSTANCE.CaysnPos_HalfCutBlackMarkPaper(mPointer);
            mCallBack.onPrintResult(true);
        } catch (Exception e) {
            mCallBack.onPrintResult(false);
            e.printStackTrace();
        }
    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return mPointer == null;
    }

    private void printLine(int line) {
        printerlibs_caysnpos.INSTANCE.CaysnPos_FeedLine(mPointer, line);
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
