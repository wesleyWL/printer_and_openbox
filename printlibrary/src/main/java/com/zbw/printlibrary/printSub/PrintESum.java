package com.zbw.printlibrary.printSub;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.widget.Toast;

import com.android.server.serial.ISerialPortService;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2020-10-17 9:46
 * 这个类是干嘛的？：PrintESum
 */
public class PrintESum extends PrinterControlCommon {

    private Context mContext;
    private IPrintCallBack mCallBack;
    private ISerialPortService iSerialPortService;

    public PrintESum(Context context, IPrintCallBack callBack) {
        mContext = context;
        mCallBack = callBack;
    }


    /**
     * 初始化
     */
    private void init() {
        if (iSerialPortService == null) {
            try {
                Intent intent = new Intent("com.android.server.serial");
                intent.setPackage("com.android.serialchat");
                ComponentName componentName = new ComponentName("com.android.serialchat", "com.android.server.serial.SerialPortService");
                intent.setComponent(componentName);
                mContext.bindService(intent, connService, Context.BIND_AUTO_CREATE);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, "打印初始化失败，请确认机型是否正确", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iSerialPortService = ISerialPortService.Stub.asInterface(service);
            try {
                if(iSerialPortService!=null) {
                    iSerialPortService.turnOnPrinter();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mCallBack.onConnect("连接成功",true);
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            iSerialPortService = null;
            mCallBack.onDisConnect("断开成功",true);
        }
    };

    @Override
    public void connectPrinter() {
        //1.启动打印机
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disConnectPrinter() {
        try {
            iSerialPortService.closeSerialPort();
            iSerialPortService.turnOffPrinter();
            mContext.unbindService(connService);
            mCallBack.onDisConnect("断开成功",true);
            if (mContext != null) {
                mContext = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onDisConnect("断开失败",false);
        }
    }

    @Override
    public void printTest(List<String> testList) {
        if(iSerialPortService == null){
            Toast.makeText(mContext,"打印服务没有唤醒",Toast.LENGTH_LONG).show();
            return;
        }

        try {
            iSerialPortService.openSerialPort();
            iSerialPortService.setPrinterMargin(8);
            for (int i = 0; i < testList.size(); i++) {
                if(i == 0){
                    //设置打印文字大小，type参数三种。0：正常大小；1:倍高 2:倍宽（默认0）
                    iSerialPortService.setPrinterFont(1);
                    //设置文字对齐方式，type参数三种。0：左对齐；1：居中；2:右对齐（默认0）
                    iSerialPortService.setPrinterLayout (1);
                }else if(i == 1){
                    //设置打印文字大小，type参数三种。0：正常大小；1:倍高 2:倍宽（默认0）
                    iSerialPortService.setPrinterFont(0);
                    //设置文字对齐方式，type参数三种。0：左对齐；1：居中；2:右对齐（默认0）
                    iSerialPortService.setPrinterLayout (1);
                }else{
                    //设置打印文字大小，type参数三种。0：正常大小；1:倍高 2:倍宽（默认0）
                    iSerialPortService.setPrinterFont(0);
                    //设置文字对齐方式，type参数三种。0：左对齐；1：居中；2:右对齐（默认0）
                    iSerialPortService.setPrinterLayout (0);
                }
                iSerialPortService.printUnicodeString(testList.get(i)+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printString(List<AttributeBean> testList) {
        if(iSerialPortService == null){
            Toast.makeText(mContext,"打印服务没有唤醒",Toast.LENGTH_LONG).show();
            return;
        }
        try {
            iSerialPortService.openSerialPort();
            iSerialPortService.setPrinterMargin(8);
            for (int i = 0; i < testList.size(); i++) {
                //设置打印文字大小，type参数三种。0：正常大小；1:倍高 2:倍宽（默认0）
                iSerialPortService.setPrinterFont(testList.get(i).getFont_size());
                //设置文字对齐方式，type参数三种。0：左对齐；1：居中；2:右对齐（默认0）
                iSerialPortService.setPrinterLayout (testList.get(i).getFont_gray());
                iSerialPortService.printUnicodeString(testList.get(i).getContext()+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        if (iSerialPortService == null) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
