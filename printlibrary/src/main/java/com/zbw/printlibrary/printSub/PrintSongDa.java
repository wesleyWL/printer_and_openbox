package com.zbw.printlibrary.printSub;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.szsicod.print.escpos.PrinterAPI;
import com.szsicod.print.io.InterfaceAPI;
import com.szsicod.print.io.USBAPI;
import com.szsicod.print.io.UsbNativeAPI;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2019-12-19 14:46
 * 这个类是干嘛的？：PrintSongDa
 * 松达打印机
 */
public class PrintSongDa extends PrinterControlCommon {
    // 松达打印
    private Context mContext;
    private IPrintCallBack mCallBack;
    private UsbBroadCastReceiver mUsbBroadCastReceiver;
    private PrinterAPI mPrinter = PrinterAPI.getInstance();

    public PrintSongDa(Context context, IPrintCallBack callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
    }

    @Override
    public void connectPrinter() {
        if (mUsbBroadCastReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            mUsbBroadCastReceiver = new UsbBroadCastReceiver();
            mContext.registerReceiver(mUsbBroadCastReceiver, intentFilter);
        }
        InterfaceAPI io = new USBAPI(mContext);
        mPrinter.connect(io);
    }

    @Override
    public void disConnectPrinter() {
        try {
            if (mPrinter != null) {
                mPrinter.disconnect();
                mPrinter = null;
            }
            if (mUsbBroadCastReceiver != null)
                mContext.unregisterReceiver(mUsbBroadCastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void printTest(final List<String> liststr) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    //居中
                    mPrinter.setAlignMode(1);
                    //设置字体样式
                    mPrinter.setFontStyle(0);
                    //设置行间距
                    mPrinter.setLineSpace(30);
                    int status = 0;
                    //设置字体大小
                    mPrinter.setCharSize(1, 1);
                    mPrinter.printString(liststr.get(0), "GBK", true);
                    mPrinter.setCharSize(0, 0);
                    mPrinter.printString(liststr.get(1), "GBK", true);

                    //恢复
                    // mPrinter.setDefaultLineSpace();
                    //靠左
                    mPrinter.setAlignMode(0);
                    for (int i = 2; i < liststr.size(); i++) {
                        status = mPrinter.printString(liststr.get(i), "GBK", true);
                    }
                    //切纸
                    mPrinter.cutPaper(66, 0);
                    if (status == 0) {
                        mCallBack.onPrintResult(true);
                    } else {
                        mCallBack.onPrintResult(false);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });

    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    //设置字体样式
                    mPrinter.setFontStyle(0);
                    //设置行间距
                    mPrinter.setLineSpace(30);
                    int status = 0;

                    for (int i = 0; i < testList.size(); i++) {
                        //居中
                        mPrinter.setAlignMode(testList.get(i).getFont_gray());
                        //设置字体大小
                        mPrinter.setCharSize(testList.get(i).getFont_size(), testList.get(i).getFont_size());
                        status = mPrinter.printString(testList.get(i).getContext(), "GBK", true);
                    }
                    //切纸
                    mPrinter.cutPaper(66, 0);
                    if (status == 0) {
                        mCallBack.onPrintResult(true);
                    } else {
                        mCallBack.onPrintResult(false);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        //byte[] bits = new byte[]{27, 112, 0, 64, 80};
        mPrinter.openCashDrawer(0,64,80);
    }

    @Override
    public boolean isConnected() {
        return false;
    }



    //usb 监听 由于权限问题而且sdk内部有超时时间 ,适用情况应该是系统默认usb权限开放或者root 板则使用UsbNativeApi
    private class UsbBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (usbDevice != null) {
                    PrinterAPI.getInstance().disconnect();
                    if (!PrinterAPI.getInstance().isConnect()) {
                        PrinterAPI.getInstance().disconnect();
                        mCallBack.onDisConnect("断开连接",true);
                    }
                    mCallBack.onDisConnect("连接失败",true);
                }
            } else if (action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                if (PrinterAPI.getInstance().isConnect()) return;

                USBAPI usbapi = new USBAPI(mContext);
                if (PrinterAPI.getInstance().connect(usbapi) == PrinterAPI.SUCCESS) {
                    mCallBack.onConnect("连接成功",true);
                } else {
                    mCallBack.onDisConnect("连接失败",false);
                }
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
