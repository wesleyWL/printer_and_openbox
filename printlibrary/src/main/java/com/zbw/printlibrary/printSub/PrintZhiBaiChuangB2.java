package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.printsdk.cmd.PrintCmd;
import com.zbw.printlibrary.aclas.ComBean;
import com.zbw.printlibrary.aclas.SerialHelper;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.CheckUtil;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 智百川-B2设备打印
 */
public class PrintZhiBaiChuangB2 extends PrinterControlCommon {
    public static String CHUAN_KOU = "/dev/ttyUSB0";   //串口类型
    public static String BAUDRATE = "38400";   //波特率

    private Context mContext;
    private IPrintCallBack mCallBack;
    private SerialControl mSerialControl;//串口

    public PrintZhiBaiChuangB2(@NonNull Context mContext, @NonNull IPrintCallBack mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        mSerialControl = new SerialControl();
        mSerialControl.setbLoopData(PrintCmd.GetStatus4());
    }

    @Override
    public void connectPrinter() {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                mSerialControl.setPort(CHUAN_KOU);
                mSerialControl.setBaudRate(BAUDRATE);
                try {
                    mSerialControl.open();
                    mCallBack.onConnect("连接成功", true);
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallBack.onConnect("连接失败", false);
                }
            }
        });
    }

    @Override
    public void disConnectPrinter() {
        try {
            if (mSerialControl != null) {
                mSerialControl.stopSend();
                mSerialControl.close();
                mCallBack.onConnect("断开成功", true);
            } else {
                mCallBack.onConnect("断开失败", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onConnect("断开失败", false);
        }
    }

    @Override
    public void printTest(final List<String> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    doPrint(testList);
                    mCallBack.onPrintResult(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printString(final List<AttributeBean> testList) {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                try {
                    doPrintString(testList);
                    mCallBack.onPrintResult(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallBack.onPrintResult(false);
                }
            }
        });
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return mSerialControl.isOpen();
    }

    private static class SerialControl extends SerialHelper {
        public SerialControl() {
        }

        @Override
        protected void onDataReceived(final ComBean ComRecData) {
//            DispQueue.AddQueue(ComRecData);// 线程定时刷新显示(推荐)
        }
    }

    private void doPrint(List<String> textList) {
        if (CheckUtil.isEmpty(textList)) {
            return;
        }
        try {
            String title = textList.get(0);
            mSerialControl.send(PrintCmd.SetAlignment(1));
            mSerialControl.send(PrintCmd.SetSizetext(1, 1));
            mSerialControl.send(PrintCmd.PrintString(title, 0));
            mSerialControl.send(PrintCmd.SetAlignment(0));
            mSerialControl.send(PrintCmd.SetSizetext(0, 0));
            for (int i = 1; i < textList.size(); i++) {
                // 小票主要内容
                mSerialControl.send(PrintCmd.PrintString(textList.get(i), 0));
            }
            // 走纸4行,再切纸,清理缓存
            printFeedCutpaper(8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doPrintString(List<AttributeBean> textList) {
        if (CheckUtil.isEmpty(textList)) {
            return;
        }
        try {
            for (int i = 0; i < textList.size(); i++) {
                // 小票主要内容
                mSerialControl.send(PrintCmd.SetAlignment(textList.get(i).getFont_gray()));
                mSerialControl.send(PrintCmd.SetSizetext(textList.get(i).getFont_size(), textList.get(i).getFont_size()));
                mSerialControl.send(PrintCmd.PrintString(textList.get(i).getContext(), 0));
            }
            // 走纸4行,再切纸,清理缓存
            printFeedCutpaper(8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 走纸换行，再切纸，清理缓存
    private void printFeedCutpaper(int iLine) {
        try {
            mSerialControl.send(PrintCmd.PrintFeedline(iLine));
            mSerialControl.send(PrintCmd.PrintCutpaper(0));
            mSerialControl.send(PrintCmd.SetClean());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
