package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;

import com.szsicod.print.escpos.PrinterAPI;
import com.szsicod.print.io.InterfaceAPI;
import com.szsicod.print.io.USBAPI;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.PrintTextUtil;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 中科英泰-60设备打印
 */
public class PrintYingTai60 extends PrinterControlCommon {
    private Context mContext;
    private IPrintCallBack mCallBack;
    private PrinterAPI mPrinter;

    public PrintYingTai60(@NonNull Context mContext, @NonNull IPrintCallBack mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        mPrinter = PrinterAPI.getInstance();
    }

    @Override
    public void connectPrinter() {
        PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
                InterfaceAPI io = new USBAPI(mContext);
                int connect = mPrinter.connect(io);
                if (connect == PrinterAPI.SUCCESS) {
                    mCallBack.onConnect("连接成功", true);
                } else {
                    mCallBack.onDisConnect("连接失败", false);
                }
            }
        });
    }

    @Override
    public void disConnectPrinter() {
        try {
            int disconnect = mPrinter.disconnect();
            if (disconnect == PrinterAPI.SUCCESS) {
                mCallBack.onDisConnect("断开成功", true);
            } else {
                mCallBack.onDisConnect("断开失败", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mCallBack.onDisConnect("断开失败", false);
        }
    }

    @Override
    public void printTest(List<String> testList) {
        String printText = "";
        printText += PrintTextUtil.headCenter(testList.get(0), 48) + "\n";
        for (int i = 1; i < testList.size(); i++) {
            printText += (testList.get(i) + "\n");
        }
        printText += "\n";
        doPrint(printText);
    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return mPrinter.isConnect();
    }

    private void doPrint(String text) {
        try {
            int resultCode = mPrinter.printString(text, "GBK", true);
            if (PrinterAPI.SUCCESS == resultCode) {
                mPrinter.cutPaper(66, 0);
            }
            //打印结果
            String statusStr = String.valueOf(mPrinter.getStatus());
            if (statusStr.length() < 3) {
                return;
            }

            if ("11".equals(statusStr.substring(1, 3))) {
                Log.e("tag", "-------打印失败,缺纸-------");
                if (mCallBack != null) {
                    mCallBack.onPrintResult(false);
                }
            } else {
                Log.d("tag", "-------打印成功-------");
                if (mCallBack != null) {
                    mCallBack.onPrintResult(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallBack != null) {
                mCallBack.onPrintResult(false);
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
