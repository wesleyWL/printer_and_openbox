package com.zbw.printlibrary.printSub;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import androidx.annotation.NonNull;
import com.example.testapplication.BLSManagerService;
import com.example.testapplication.PrintData;
import com.example.testapplication.PrintService;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * 作者 ：Wesley
 * 时间 ：2023-04-25 10:07
 * PrintBalance 说明：
 * 佰伦斯打印
 */
public class PrintBalance implements IPrinterControl {
    private BLSManagerService mService;
    private PrintService mPrintService;
    private Intent intent;
    private Context mContext;
    private final IPrintCallBack mCallBack;

    public PrintBalance(@NonNull Context context, IPrintCallBack callback) {
        mContext = context;
        this.mCallBack = callback;
    }

    @Override
    public void connectPrinter() {
        if(mService == null){
            intent = new Intent();
            intent.setPackage("com.example.testapplication");
            intent.setAction("com.example.testapplication.services.BlsManagerService");
            mContext.bindService(intent, connService, Context.BIND_AUTO_CREATE);
        }
    }


    public ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mPrintService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = BLSManagerService.Stub.asInterface(service);
            try{
                mPrintService = PrintService.Stub.asInterface(mService.getPrintService());
                if(mPrintService != null) {
                    mPrintService.initPrinter();
                }
            }catch (Exception e){
               e.printStackTrace();
            }
        }
    };

    @Override
    public void disConnectPrinter() {
        mContext.unbindService(connService);
    }

    @Override
    public void printTest(List<String> testList) {
        if(mPrintService != null) {
            try {
                mPrintService.print(testList);
            } catch (Exception e) {
                mCallBack.onPrintResult(false);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList) {
        List<PrintData> data = conversion(testList);
        if(mPrintService != null) {
            try {
                mPrintService.printWithAttribute(data);
            } catch (Exception e) {
                mCallBack.onPrintResult(false);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {
        printString(testList);
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        if(mPrintService != null) {
            try {
                mPrintService.openMoneyBox();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    public static List<PrintData> conversion(List<AttributeBean> data){
        List<PrintData> printList = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            AttributeBean info = data.get(i);
            PrintData printData = new PrintData();
            printData.setBitmap(info.getBitmap());
            printData.setFont_size(info.getFont_size());
            printData.setFont_gray(info.getFont_gray());
            printData.setBold(info.isBold());
            printData.setFont_line(info.isFont_line());
            printData.setContext(info.getContext());
            printData.setContext_type(info.getContext_type());
            printList.add(printData);
        }
        return printList;
    }
}
