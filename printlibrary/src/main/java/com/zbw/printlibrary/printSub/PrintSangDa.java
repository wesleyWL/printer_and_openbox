package com.zbw.printlibrary.printSub;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.szsicod.print.escpos.PrinterAPI;
import com.szsicod.print.io.InterfaceAPI;
import com.szsicod.print.io.SerialAPI;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import com.zbw.printlibrary.util.PrintThreadPoolManager;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 桑达设备打印
 */
public class PrintSangDa extends PrinterControlCommon {
    static {
        System.loadLibrary("serial_port_sangda");
    }

    private Context mContext;
    private IPrintCallBack mCallBack;
    private PrinterAPI mPrinter;

    public PrintSangDa(@NonNull Context mContext, @NonNull IPrintCallBack mCallBack) {
        this.mContext = mContext;
        this.mCallBack = mCallBack;
        mPrinter = PrinterAPI.getInstance();
    }

    @Override
    public void connectPrinter() {
        try {
            PrintThreadPoolManager.getInstance().executeTask(new Runnable() {
                @Override
                public void run() {
                    InterfaceAPI io = new SerialAPI(new File("/dev/ttyS1"), 38400, 0);
                    if (mPrinter.connect(io) == 0) {
                        if (mCallBack != null) {
//                            mCallBack.onConnect("连接成功", true);
                            mCallBack.onConnect(mPrinter.isConnect()?"连接成功":"连接失败", true);
                        }
                    } else {
                        if (mCallBack != null) {
                            mCallBack.onConnect("连接失败", false);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallBack != null) {
                mCallBack.onConnect("连接失败", false);
            }

        }
    }

    @Override
    public void disConnectPrinter() {
        try {
            mPrinter.disconnect();
            if (mCallBack != null) {
                mCallBack.onDisConnect("断开成功", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallBack != null) {
                mCallBack.onDisConnect("断开失败", false);
            }
        }
    }

    @Override
    public void printTest(List<String> testList) {
        try {
            if (!mPrinter.isConnect()) return;
            String title = testList.get(0);
            mPrinter.setCharSize(1, 1);
            mPrinter.setAlignMode(1);
//            mPrinter.printString(title, "GBK", true);
            mPrinter.printString(title, "GBK", true);
            mPrinter.setCharSize(0, 0);
            mPrinter.setAlignMode(0);
            for (int i = 1; i < testList.size(); i++) {
                mPrinter.printString(testList.get(i), "GBK", true);
            }
            printLine(3);
            mPrinter.cutPaper(66, 0);
            if (mCallBack != null) {
                mCallBack.onPrintResult(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallBack != null) {
                mCallBack.onPrintResult(false);
            }
        }
    }

    @Override
    public void printString(List<AttributeBean> testList) {
        try {
            for (int i = 1; i < testList.size(); i++) {
                mPrinter.setCharSize(testList.get(i).getFont_size(), testList.get(i).getFont_size());
                mPrinter.setAlignMode(testList.get(i).getFont_gray());
                mPrinter.printString(testList.get(i).getContext(), "GBK", true);
            }
            printLine(3);
            mPrinter.cutPaper(66, 0);
            if (mCallBack != null) {
                mCallBack.onPrintResult(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallBack != null) {
                mCallBack.onPrintResult(false);
            }
        }
    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapHashMap) {
        printTest(testList);
    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {

    }

    @Override
    public boolean isConnected() {
        return mPrinter.isConnect();
    }

    private void printLine(int line) throws UnsupportedEncodingException {
        for (int i = 0; i < line; i++) {
            mPrinter.printString(" ", "GBK", true);
        }
    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

}
