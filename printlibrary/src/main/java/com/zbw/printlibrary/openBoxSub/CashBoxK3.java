package com.zbw.printlibrary.openBoxSub;

import android.graphics.Bitmap;
import com.hcd.hcdpos.cashbox.Cashbox;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2024-09-13 16:54
 * 这个类是干嘛的？：CashboxK3
 */
public class CashBoxK3 extends PrinterControlCommon {

    @Override
    public void connectPrinter() {

    }

    @Override
    public void disConnectPrinter() {

    }

    @Override
    public void printTest(List<String> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {

    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        try {
            Cashbox.doOpenCashBox();
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            Cashbox.doOpenCashBoxVol();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
