package com.zbw.printlibrary.openBoxSub;

import android.graphics.Bitmap;

import com.factory.jni.JniUtil;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2024-09-13 18:00
 * 这个类是干嘛的？：CashBoxK5
 */
public class CashBoxK5 extends PrinterControlCommon {
    private JniUtil mJniUtil;
    public CashBoxK5() {
        mJniUtil = new JniUtil();
    }

    @Override
    public void connectPrinter() {

    }

    @Override
    public void disConnectPrinter() {

    }

    @Override
    public void printTest(List<String> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {

    }

    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        try {
            if (mJniUtil != null){
                mJniUtil.openCashBox();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
