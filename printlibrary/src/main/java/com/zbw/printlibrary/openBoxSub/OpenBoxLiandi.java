package com.zbw.printlibrary.openBoxSub;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.landicorp.financekit.cashbox.CashBoxBinder;
import com.landicorp.financekit.cashbox.ICashBox;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by cyj on 2019-08-05.
 * 联迪开钱箱
 */

public class OpenBoxLiandi extends PrinterControlCommon {

    private ICashBox mCashBoxService;
    private Context mContext;
    private IPrintCallBack mCallBack;

    public OpenBoxLiandi(@NonNull Context context, IPrintCallBack callback) {
        mContext = context.getApplicationContext();
        this.mCallBack = callback;
    }

    @Override
    public void connectPrinter() {
        if (mCashBoxService == null) {
            try {
                Intent intent = new Intent();
                CashBoxBinder.bindService(mContext,intent, connService, Context.BIND_AUTO_CREATE);
                mCallBack.onConnect("连接成功",true);
            }catch (Exception e){
                Toast.makeText(mContext, "没有找到联迪模块", Toast.LENGTH_SHORT).show();
                mCallBack.onConnect("连接失败",false);
                e.printStackTrace();
            }

        }
    }

    @Override
    public void disConnectPrinter() {
        try {
            CashBoxBinder.unBindService(mContext.getApplicationContext(),
                    connService);
            mCallBack.onDisConnect("断开成功",true);
        }catch (Exception e) {
            e.printStackTrace();
            mCallBack.onDisConnect("断开失败",false);
        }
    }

    @Override
    public void printTest(List<String> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {

    }


    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }


    @Override
    public void openMoneyBox() {
        //开钱箱
        try{
            if (mCashBoxService!=null){
                mCashBoxService.openBox();
            }
        }catch (RemoteException e){
            Toast.makeText(mContext,"没有找到联迪模块",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean isConnected() {
        return false;
    }


    public ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCashBoxService = null;
            mCallBack.onDisConnect("断开成功",true);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mCashBoxService = ICashBox.Stub.asInterface(service);
            mCallBack.onConnect("连接成功",true);
        }
    };
}
