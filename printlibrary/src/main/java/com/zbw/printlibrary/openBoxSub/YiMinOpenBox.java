package com.zbw.printlibrary.openBoxSub;

import android.graphics.Bitmap;

import com.imin.library.IminSDKManager;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrinterControl;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2023-12-25 10:17
 * 这个类是干嘛的？：YiMinOpenBox
 */
public class YiMinOpenBox implements IPrinterControl {

   public YiMinOpenBox() {
   }

   @Override
   public void connectPrinter() {

   }

   @Override
   public void disConnectPrinter() {

   }

   @Override
   public void printTest(List<String> testList) {

   }

   @Override
   public void printString(List<AttributeBean> testList) {

   }

   @Override
   public void printString(List<AttributeBean> testList, int width) {

   }

   @Override
   public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {

   }

   @Override
   public void blueConnect(String address) {

   }

   @Override
   public void disBlueConnect() {

   }

   @Override
   public void openMoneyBox() {
      IminSDKManager.opencashBox();
   }

   @Override
   public boolean isConnected() {
      return false;
   }
}
