package com.zbw.printlibrary.openBoxSub;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

import com.posin.device.CashDrawer;
import com.zbw.printlibrary.bean.AttributeBean;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.PrinterControlCommon;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * 宝盈开钱箱
 */

public class CashboxPostin extends PrinterControlCommon {
    // 钱箱设备实例
    CashDrawer mCashDrawer = null;
    private Context mContext;
    private IPrintCallBack mCallBack;

    public CashboxPostin(Context context, IPrintCallBack callback) {
        mContext = context;
        this.mCallBack = callback;
    }
    @Override
    public void connectPrinter() {
        try {
            mCashDrawer = CashDrawer.newInstance();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Toast.makeText(mContext, "宝盈开钱箱初始化失败", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void disConnectPrinter() {

    }

    @Override
    public void printTest(List<String> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList) {

    }

    @Override
    public void printString(List<AttributeBean> testList, int width) {

    }

    @Override
    public void printStringAndBitmap(List<String> testList, LinkedHashMap<Integer, Bitmap> bitmapMap) {

    }


    @Override
    public void blueConnect(String address) {

    }

    @Override
    public void disBlueConnect() {

    }

    @Override
    public void openMoneyBox() {
        if (mCashDrawer!=null) {
            mCashDrawer.kickOutPin2(100);
        }
    }

    @Override
    public boolean isConnected() {
        if (mCashDrawer==null){
            return false;
        }else{
            return true;
        }

    }


}
