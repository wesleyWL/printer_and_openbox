package com.zbw.printlibrary.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2018/8/13.
 */

public class PrintThreadPoolManager {
    private ExecutorService service;
    private static final PrintThreadPoolManager manager = new PrintThreadPoolManager();

    private PrintThreadPoolManager() {
        //Runtime.getRuntime().availableProcessors() 为cpu线程数量
        //int num = Runtime.getRuntime().availableProcessors() * 1;

        //指定单线程打印，防止同时多份小票打印，造成小票内容错乱
        this.service = Executors.newFixedThreadPool(1);
    }

    public static PrintThreadPoolManager getInstance() {
        return manager;
    }

    public void executeTask(Runnable runnable) {
        this.service.execute(runnable);
    }
}
