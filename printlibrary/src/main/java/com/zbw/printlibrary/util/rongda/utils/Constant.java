package com.zbw.printlibrary.util.rongda.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Constant {
    public static final String SCALE_COM_PATH = "/dev/ttysWK3";  //ttyS4
    public static final int SCALE_BPS_19200 = 19200;
    public static final int CMD_TYPE_ESC = 101;
    public static final int CMD_TYPE_TSC = 102;

    public static final int STATUS_PRINTER_CONNECT = 1001;
    public static final int STATUS_SCALE_CONNECT = 1002;
    public static final int STATUS_PRINTER_DISCONNECT = 1003;
    public static final int STATUS_SCALE_DISCONNECT = 1004;

    public static final int MSG_SCALE_PARSE = 2001;

    public static final int DEVTYPE_PRINTER=0;//打印机
    public static final int DEVTYPE_SCALE=1;//秤

    @Retention(RetentionPolicy.SOURCE)
    public @interface ConnScaleType { //连接秤类型
        int Serial_Port = 0;//串口
        int usb_Port = 1;// usb
        int Bluetooth_Port = 2;//蓝牙


    }

}
