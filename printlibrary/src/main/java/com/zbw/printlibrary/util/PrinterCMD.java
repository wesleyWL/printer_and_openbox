package com.zbw.printlibrary.util;

/**
 * Created by shy on 2017/6/5.
 * 厨房-通用型打印机的切纸帮助类
 */

public class PrinterCMD {

    public  static PrinterCMD pritcmd = null;
    //单例模式中获取唯一的App实例
    public static PrinterCMD getPritcmd() {
        if (null == pritcmd) {
            pritcmd = new PrinterCMD();
        }
        return pritcmd;
    }



    /// <summary>
    /// 换行（回车）
    /// </summary>
    /// <returns></returns>
    public String CMD_Enter()
    {
        return new StringBuffer().append((char)10).toString();
    }


    /// <summary>
    /// 走纸
    /// </summary>
    /// <param name="line">走纸的行数</param>
    /// <returns></returns>
    public String CMD_PageGO(int line)
    {
        return new StringBuffer().append((char)27).append((char)100).append((char)line).toString();
    }

    /// <summary>
    /// 切割
    /// </summary>
    /// <returns></returns>
    public String CMD_CutPage()
    {
        return new StringBuffer().append((char)27).append((char)109).toString();
    }


}
