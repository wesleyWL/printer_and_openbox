package com.zbw.printlibrary.util.rongda;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.rt.printerlibrary.cmd.Cmd;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.enumerate.BarcodeStringPosition;
import com.rt.printerlibrary.enumerate.BarcodeType;
import com.rt.printerlibrary.enumerate.BmpPrintMode;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.enumerate.ESCBarcodeFontTypeEnum;
import com.rt.printerlibrary.enumerate.ESCFontTypeEnum;
import com.rt.printerlibrary.enumerate.SettingEnum;
import com.rt.printerlibrary.exception.SdkException;
import com.rt.printerlibrary.factory.cmd.CmdFactory;
import com.rt.printerlibrary.setting.BarcodeSetting;
import com.rt.printerlibrary.setting.BitmapSetting;
import com.rt.printerlibrary.setting.CommonSetting;
import com.rt.printerlibrary.setting.TextSetting;

import java.io.UnsupportedEncodingException;

/**
 * ESC指令管理类
 *
 */
public class ESCCmdManager {

    private Cmd escCmd ;
    private TextSetting textSetting;
    private CommonSetting commonSetting;
    private BitmapSetting bitmapSetting;
    private BarcodeSetting bcSetting ;
    private String CHARTSET = "GBK";

    public ESCCmdManager() {
        CmdFactory escFac = new EscFactory();
        escCmd = escFac.create();
        escCmd.append(escCmd.getHeaderCmd());//初始化, Initial
        escCmd.setChartsetName(CHARTSET);
        textSetting = new TextSetting();
        commonSetting = new CommonSetting();
        bitmapSetting = new BitmapSetting();
        bcSetting = new BarcodeSetting();
    }

    /**
     * 打印自测页指令 self test
     *
     * @return
     */
    public ESCCmdManager selftestPage(){
        escCmd.append(escCmd.getSelfTestCmd());
        return this;
    }

    /**
     * 重置 reset
     *
     * @return
     */
    public ESCCmdManager resetCommand(){
        escCmd.clear();
        textSetting = new TextSetting();
        commonSetting = new CommonSetting();
        bitmapSetting = new BitmapSetting();
        return this;
    }

    /**
     * 设置ChartSet set char set
     *
     * @param chartSet
     * @return
     */
    public ESCCmdManager setTextChartSet(String chartSet){
        CHARTSET = chartSet;
        return this;
    }

    //########################条码##########################################
    /**
     * 设置条码文字位置 set barcode text position
     *
     * @param position @link BarcodeStringPosition
     * @return
     */
    public ESCCmdManager setBcTextPosition(BarcodeStringPosition position){
        bcSetting.setBarcodeStringPosition(position);
        return this;
    }

    /**
     * 设置条码长和宽 set barcode width and height (unit:dot ; 1mm = 8dot)
     * 以点（dot）为单位 unit:dot  1mm = 8dot
     *
     * @param wValue range : 2~6 ; default:3
     * @param hValue range : 1~255 default:72 (unit:dot)
     * @return
     */
    public ESCCmdManager setBcWideAndHeight(int wValue , int hValue) {
        bcSetting.setBarcodeWidth(wValue);
        bcSetting.setHeightInDot(hValue);
        return this;
    }

    /**
     * 设置barcode字体类型 set barcode text font
     *
     * @param font
     * @return
     */
    public ESCCmdManager setBcFont(ESCBarcodeFontTypeEnum font){
        bcSetting.setEscBarcodFont(font);
        return this;
    }

    /**
     * 添加条码文本 add a barcode
     *
     * @param barcodeType
     * @param content
     * @return
     */
    public ESCCmdManager addBarcode(BarcodeType barcodeType, String content){
        escCmd.append(escCmd.getCommonSettingCmd(commonSetting));
        try {
            escCmd.append(escCmd.getBarcodeCmd(barcodeType, bcSetting, content));
        } catch (SdkException e) {
            e.printStackTrace();
        }
        return this;
    }
    //########################条码##########################################
    /**
     * barcode or qrcode align left 左对齐
     *
     * @return
     */
    public ESCCmdManager setBcQrCodeAlignLeft(){
        bcSetting.setAlign(CommonEnum.ALIGN_LEFT);
        return this;
    }

    /**
     * barcode or qrcode align center 居中
     *
     * @return
     */
    public ESCCmdManager setBcQrCodeAlignCenter(){
        bcSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
        return this;
    }

    /**
     * barcode or qrcode align right右对齐
     *
     * @return
     */
    public ESCCmdManager setBcQrCodeAlignRight() {
        bcSetting.setAlign(CommonEnum.ALIGN_RIGHT);
        return this;
    }
    //########################二维码##########################################
    /**
     * 添加二维码文本并设置大小 add qrcode
     *
     * @param content 文本
     * @param size 二维码宽度 范围 1~15 range:1-15
     * @return
     */
    public ESCCmdManager addQrCode(String content, int size){
        try {
            bcSetting.setQrcodeDotSize(size);
            escCmd.append(escCmd.getBarcodeCmd(BarcodeType.QR_CODE, bcSetting, content));
        } catch (SdkException e) {
            e.printStackTrace();
        }
        return this;
    }
    //########################二维码##########################################
    /**
     * align left左对齐
     *
     * @return
     */
    public ESCCmdManager setAlignLeft(){
        commonSetting.setAlign(CommonEnum.ALIGN_LEFT);
        return this;
    }

    /**
     * align center居中
     * @return
     */
    public ESCCmdManager setAlignCenter(){
        commonSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
        return this;
    }

    /**
     * align right右对齐
     * @return
     */
    public ESCCmdManager setAlignRight() {
        commonSetting.setAlign(CommonEnum.ALIGN_RIGHT);
        return this;
    }

    /**
     * 设置字体 set text font
     *
     * @param value
     * @return
     */
    public ESCCmdManager setTextFont(ESCFontTypeEnum value) {
        textSetting.setEscFontType(value);
        return this;
    }
    //########################图片##########################################
    /**
     * 图片处理模式 set bitmap print mode
     *
     * @param bmpCmdMode
     * @return
     */
    public ESCCmdManager setBmpCmdMode(BmpPrintMode bmpCmdMode) {
        bitmapSetting.setBmpPrintMode(bmpCmdMode);
        return this;
    }

    /**
     * 图片宽度限制设置 set limit width of bitmaps
     *
     * @param width 58 printer: width <=48 ; 80 printer : width <=72
     * @return
     */
    public ESCCmdManager setBmpLimitWidth(int width) {
        bitmapSetting.setBimtapLimitWidth(width*8);
        return this;
    }

    /**
     * 58宽度打印纸，图片最大宽度  max width for 58 printer
     *
     * @return
     */
    public ESCCmdManager setBmp58Width() {
        bitmapSetting.setBimtapLimitWidth(48*8);
        return this;
    }

    /**
     * 80宽度打印纸,图片最大宽度  max width for 80 printer
     *
     * @return
     */
    public ESCCmdManager setBmp80Width() {
        bitmapSetting.setBimtapLimitWidth(72*8);
        return this;
    }

    /**
     * 添加图片 add bitmap
     *
     * @param bitmap
     * @return
     */
    public ESCCmdManager addBmp(Bitmap bitmap) {
        escCmd.append(escCmd.getCommonSettingCmd(commonSetting));
        try {
            escCmd.append(escCmd.getBitmapCmd(bitmapSetting, bitmap));
        } catch (SdkException e) {
            e.printStackTrace();
        }
        return this;
    }
    //########################图片##########################################

    /**
     * 文本左对齐
     *
     * @return
     */
    public ESCCmdManager setTextAlignLeft(){
        textSetting.setAlign(CommonEnum.ALIGN_LEFT);
        return this;
    }

    /**
     * 文本居中
     *
     * @return
     */
    public ESCCmdManager setTextAlignCenter(){
        textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
        return this;
    }

    /**
     * 文本右对齐
     *
     * @return
     */
    public ESCCmdManager setTextAlignRight() {
        textSetting.setAlign(CommonEnum.ALIGN_RIGHT);
        return this;
    }

    /**
     * 文字粗体 text bold
     *
     * @param enable
     * @return
     */
    public ESCCmdManager setTextBold(boolean enable) {
        if(true){
            textSetting.setBold(SettingEnum.Enable);
        }else {
            textSetting.setBold(SettingEnum.Disable);
        }
        return this;
    }


    /**
     * 设置倍高倍宽 set text font double width and height
     *
     * @param enHeight
     * @param enWidth
     * @return
     */
    public ESCCmdManager setTextFontDoubleWidthHeight(boolean enHeight, boolean enWidth) {
        if(enHeight){
            textSetting.setDoubleHeight(SettingEnum.Enable);
        }else {
            textSetting.setDoubleHeight(SettingEnum.Disable);
        }
        if(enWidth){
            textSetting.setDoubleWidth(SettingEnum.Enable);
        }else {
            textSetting.setDoubleWidth(SettingEnum.Disable);
        }
        return this;
    }

    /**
     * 设置小字体
     *
     * @param enable
     * @return
     */
    public ESCCmdManager setTextSmallFont(boolean enable) {
        if(enable){
            textSetting.setIsEscSmallCharactor(SettingEnum.Enable);
        }else {
            textSetting.setIsEscSmallCharactor(SettingEnum.Disable);
        }

        return this;
    }

    /**
     * 开钱箱 open cash box
     *
     * @return
     */
    public ESCCmdManager openCashDrawer() {
        escCmd.append(escCmd.getOpenMoneyBoxCmd());
        return this;
    }
    /**
     * 切刀全切指令 cutter
     *
     * @return*/
    public ESCCmdManager useAllCutter() {
        escCmd.append(escCmd.getAllCutCmd());
        return this;
    }

    /**
     * half cutter
     *
     * @return
     */
    public ESCCmdManager useHalfCutter() {
        escCmd.append(escCmd.getHalfCutCmd());
        return this;
    }

    /**
     * 换行 line feed
     * @return
     */
    public ESCCmdManager addLineFeed() {
        escCmd.append(escCmd.getLFCRCmd());
        return this;
    }

    /**
     * 添加多个空行 muti line feeds
     *
     * @param lines
     */
    public ESCCmdManager addMutiLineFeeds(int lines) {
        for(int i = 0;i<lines;i++){
            escCmd.append(escCmd.getLFCRCmd());
        }
        return this;
    }

    /**
     * 添加打印数据 add text
     * @param msg
     */
    public ESCCmdManager addText(String msg) {

        if (TextUtils.isEmpty(msg)) {
            return this;
        }

        try {
            escCmd.append(escCmd.getTextCmd(textSetting, msg, CHARTSET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * 获取最终的数据
     * @return
     */
    public byte[] build() {
        return escCmd.getAppendCmds();
    }
}
