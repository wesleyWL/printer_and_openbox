package com.zbw.printlibrary.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * 作者 ：Wesley
 * 时间 ：2019-12-19 17:35
 * 这个类是干嘛的？：NetworkPortUtil
 */
public class NetworkPortUtil {

    /**
     * 设置字体打印位置
     * 0：居左(默认) 1：居中 2：居右
     * @param oStream
     * @param position
     */
    public static void setLocation(PrintWriter oStream, int position){
        oStream.write(0x1B);
        oStream.write(97);
        oStream.write((byte)position);
        oStream.flush();
    }

    /**
     * 设置字体大小
     * @param oStream
     * @param size 默认 8
     * 有的打印机并不是使用 0x1c 作为放大指令，而是使用 0x1b 作为放大指令，
     * 因为如果 0x1b 指令若不存在，打印机自动将其抛弃
     */
    public static void setSize(PrintWriter oStream,int size){
        oStream.write(0x1c);
        oStream.write(0x21);
        oStream.write(size);

        oStream.write(0x1b);
        oStream.write(0x21);
        oStream.write(size);
        oStream.flush();
    }


    //<param name="pagenum">切割时，走纸行数</param>
    //经过测试这种切纸指令是比较通用型的，适合佳博，钻木，二维火
    public static void CutPage(Socket socket, int pagenum) {
        try {
            OutputStream stream = socket.getOutputStream();
            PrinterCMD pcmd = PrinterCMD.getPritcmd();
            //打印命令字符串
            String command = pcmd.CMD_PageGO(pagenum) + pcmd.CMD_Enter();
            //传输的命令集
            byte[] outbytes = command.getBytes(Charset.forName("ASCII"));
            stream.write(outbytes);

            command = pcmd.CMD_CutPage() + pcmd.CMD_Enter();
            outbytes = command.getBytes(Charset.forName("ASCII"));
            stream.write(outbytes);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 蜂鸣器
     * @param socket
     * 27,66 是蜂鸣器
     * 4表示鸣叫次数
     * 1是代表鸣叫时间
     */
    public static synchronized void defaultBuzzer(Socket socket,int number,int time) {
        try {
            OutputStream stream = socket.getOutputStream();
            byte[] bits = new byte[]{27, 66, (byte) number, (byte) time};
            stream.write(bits);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 加粗
     * @param oStream
     * @param flag true 加粗 false 不加粗
     * @throws IOException
     * 默认加粗
     */
    public static void setBold(PrintWriter oStream,boolean flag) throws IOException {
        if (flag) {
            // 常规粗细
            oStream.write(0x1B);
            oStream.write(69);
            oStream.write(0xF);
            oStream.flush();
        } else {
            // 加粗
            oStream.write(0x1B);
            oStream.write(69);
            oStream.write(0);
            oStream.flush();
        }
    }

    /**
     * 设置字体大小
     * @param oStream
     * @param num 0 默认大小
     */
    public  static void fontSizeSetBig(PrintWriter oStream, int num) {
        byte realSize = 0;
        switch (num) {
            case 1:
                realSize = 0;
                break;
            case 2:
                realSize = 17;
                break;
            case 3:
                realSize = 34;
                break;
            case 4:
                realSize = 51;
                break;
            case 5:
                realSize = 68;
                break;
            case 6:
                realSize = 85;
                break;
            case 7:
                realSize = 102;
                break;
            case 8:
                realSize = 119;
                break;
        }

        oStream.write(29);
        oStream.write(33);
        oStream.write(realSize);
        oStream.flush();
    }

    /**
     * 打印绝对位置
     * @param oStream 流
     * @param hight 高
     * @param width 宽
     * @throws IOException
     */
    public static void printLocation(PrintWriter oStream, int hight, int width) throws IOException {
        oStream.write(0x1B);
        oStream.write(0x24);
        oStream.write(hight);
        oStream.write(width);
        oStream.flush();
    }




}
