package com.zbw.printlibrary.util.rongda;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;

import com.rt.printerlibrary.bean.UsbConfigBean;
import com.rt.printerlibrary.connect.PosInterface;
import com.rt.printerlibrary.factory.connect.PIFactory;
import com.rt.printerlibrary.factory.connect.UsbFactory;
import com.rt.printerlibrary.factory.printer.ThermalPrinterFactory;
import com.rt.printerlibrary.observer.PosObserver;
import com.rt.printerlibrary.posdevice.RTPosDevice;
import com.zbw.printlibrary.util.rongda.utils.Constant;

import java.util.Arrays;

/**
 * 设备管理类
 * pos manager
 *
 */
public abstract class PosManager implements PosObserver {
    private String TAG="PosManager";
    /**
     * 传入上下文
     * context
     */
    private Context mContext;
    /**
     * printerPos：打印机
     */
    private RTPosDevice printerPos;
    /**
     * 记录打印机是否连接
     *
     */
    private boolean isPrinterConncect = false;

    protected boolean isResendData=false; //需要重发
    protected byte[] latestPrintData;
    private static UsbConfigBean musbConfigBean;
    private static int mCmdtype= Constant.CMD_TYPE_ESC;
    /**
     * 获取打印机是否连接
     *get value of printer connect status
     * @return 打印机是否连接
     */
    public boolean isPrinterConncect() {
        return isPrinterConncect;
    }

    public PosManager(Context context) {
        this.mContext = context;
    }


    public void sendEscMsgToPrinter(byte[] bytes){
        if(printerPos !=null && bytes!=null){
            printerPos.writeMsg(bytes);
        }
    }


    /**
     * 断开打印机
     * disconnect printer
     */
    public void disconnectPrinter() {
        if (printerPos != null) {
            printerPos.disConnect();
        }
    }

    /**
     * 通过cmdType选择连接打印机
     * connect printer
     * @param usbConfigBean
     */
    public void connectPrinter(final UsbConfigBean usbConfigBean){
        musbConfigBean = usbConfigBean;
        if(printerPos != null && isPrinterConncect){
            disconnectPrinter();
        }
        printerPos = new ThermalPrinterFactory().create();

        UsbManager mUsbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        PIFactory piFactory = new UsbFactory();

        PosInterface posInterface = piFactory.create();
        posInterface.setConfigObject(usbConfigBean);
        if(printerPos != null) {
            printerPos.setPosInterface(posInterface);
            if (mUsbManager.hasPermission(usbConfigBean.usbDevice)) {
                try {
                    printerPos.connect(usbConfigBean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                mUsbManager.requestPermission(usbConfigBean.usbDevice, usbConfigBean.pendingIntent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            printerPos.connect(usbConfigBean);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 2000);
            }
        }
    }


    /**
     * 打印机/秤连接状态回调
     * get connection status
     * @param posInterface
     * @param status
     */
    public abstract void posStatusCallback(PosInterface posInterface, int status);

    /**
     * 打印机/ 秤消息回调
     * get msg from printer/scale
     * @param posInterface
     * @param bytes
     */
    public abstract void posMsgCallback(PosInterface posInterface,  byte[] bytes);


    /**
     * 重写连接状态接口
     * override connection status
     * @param posInterface
     * @param status
     */
    @Override
    public void posObserverCallback(PosInterface posInterface, int status) {
    }


    /**
     * 重写消息回调接口
     * override message callback
     * @param posInterface
     * @param bytes
     */
    @Override
    public void posReadMsgCallback(PosInterface posInterface, byte[] bytes) {
        posMsgCallback(posInterface,bytes);
    }

}
