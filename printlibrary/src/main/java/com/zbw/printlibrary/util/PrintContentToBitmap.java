package com.zbw.printlibrary.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import com.zbw.printlibrary.bean.AttributeBean;
import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2022-10-31 11:05
 * PrintContentToBitmap 说明：
 * 打印内容转图片
 */
public class PrintContentToBitmap {
    private static final String computeMaxWidthStr = "正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正正";
    //打印内容
    private final List<AttributeBean> printList;
    //画笔
    private final TextPaint textPaint;
    //这里需要设置图片大小
    private final int width;
    private int printHeight = 0;//高度游标

    private double cyWidth = 3;

    public PrintContentToBitmap(List<AttributeBean> printList,int paperWidth){
        this.printList = printList;
        width = paperWidth * 7;
        textPaint = new TextPaint();
        if(paperWidth>75){
            cyWidth = 3.5;
        }
    }

    /**
     * @return 计算图片高度
     */
    private int setHeight(){
        int height = 0;
        int mH = 0;
        double mMaxSize;
        double mWidthScale;
        for (AttributeBean data : printList) {
            setPaint(data);
            mWidthScale = 1.0;
            if(data.getContext_type() == 0) {
                if(!data.isFont_line() && data.getFont_gray() == 0){
                    StaticLayout lsMax = new StaticLayout(computeMaxWidthStr, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 2.0F, true);
                    mMaxSize = lsMax.getLineStart(1)+0.0;
                    mWidthScale = (mMaxSize-cyWidth)/mMaxSize;
                }

                StaticLayout ls = new StaticLayout(data.getContext(), textPaint, (int)(width*mWidthScale), Layout.Alignment.ALIGN_NORMAL, 1.0F, 2.0F, true);
                if (data.isFont_line()) {
                    height += Math.max(mH, ls.getHeight());
                    mH = 0;
                } else {
                    mH = Math.max(mH, ls.getHeight());
                }
            }else if(data.getContext_type() == 1){
                if (data.isFont_line()) {
                    height += Math.max(mH, data.getBitmap().getHeight());
                    mH = 0;
                } else {
                    mH = Math.max(mH, data.getBitmap().getHeight());
                }
            }else if(data.getContext_type() == 2){
                height += 11;
            }
            Log.e("高度",height+"");
        }
        return height;
    }

    /**
     * 打印内容转图片
     */
    public Bitmap Print() throws Exception {
        if(printList == null || printList.size() == 0){
            Log.e("PrintContentToBitmap","打印内容为空");
            throw new Exception("打印内容为空！！！");
        }
        int height = setHeight();
        //创建小票空白位图，以供后续填充内容。
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //创建画布
        Canvas canvas = new Canvas(bitmap);
        //设置背景颜色
        canvas.drawColor(Color.WHITE);
        int mH = 0;
        double mMaxSize;
        double mWidthScale;
        for (AttributeBean data : printList) {
            if(data.getContext_type() == 0){//文字
                setPaint(data);
                Layout.Alignment alignment;
                if(data.getFont_gray() == 0){
                    alignment = Layout.Alignment.ALIGN_NORMAL;
                }else if(data.getFont_gray() == 1){
                    alignment = Layout.Alignment.ALIGN_CENTER;
                }else {
                    alignment = Layout.Alignment.ALIGN_OPPOSITE;
                }
                mWidthScale = 1.0;
                if(!data.isFont_line() && data.getFont_gray() == 0){
                    StaticLayout lsMax = new StaticLayout(computeMaxWidthStr, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 2.0F, true);
                    mMaxSize = lsMax.getLineStart(1)+0.0;
                    mWidthScale = (mMaxSize-cyWidth)/mMaxSize;
                }

                StaticLayout ls = new StaticLayout(data.getContext(), textPaint, (int)(width*mWidthScale), alignment, 1.0F, 2.0F, true);
                canvas.save();
                canvas.translate(0,printHeight);
                mH = Math.max(mH,ls.getHeight());
                ls.draw(canvas);
                canvas.restore();
            } else if(data.getContext_type() == 1){//图片
                //重置paint
                textPaint.reset();
                int btX;
                if(data.getFont_gray() == 0){//居左
                     btX = 10;//padding left 10
                }else if(data.getFont_gray() == 1){//居中
                    btX = (width-data.getBitmap().getWidth())/2;
                }else {//居右
                    btX = width-data.getBitmap().getWidth() - 10;//padding right 10
                }
                canvas.drawBitmap(data.getBitmap(), btX, printHeight, textPaint);
                mH = Math.max(mH,data.getBitmap().getHeight());
            }else if(data.getContext_type() == 2){//分割线
                textPaint.setStyle(Paint.Style.STROKE);
                textPaint.setStrokeWidth(2);
                //设置字体颜色
                textPaint.setColor(Color.BLACK);
                //设置是否抗锯齿
                textPaint.setAntiAlias(true);
                printHeight += 5;
                canvas.drawLine(10, printHeight, width - 10, printHeight, textPaint);
                printHeight += 5;
            }

            if(data.isFont_line() || data.getContext_type() == 2){
                printHeight += mH;
                mH = 0;
            }
        }
        return bitmap;
    }

    private void setPaint(AttributeBean data){
        //重置paint
        textPaint.reset();
        //设置字体大小
        if(data.isBold()){
            //字体加粗
            textPaint.setFakeBoldText(true);
        }
        if(data.getFont_size() == 0){//小字体
            textPaint.setTextSize(24);
        }else if(data.getFont_size() == 1){//中字体
            textPaint.setTextSize(40);
        }else {//大字体
            textPaint.setTextSize(56);
        }
        //设置字体颜色
        textPaint.setColor(Color.BLACK);
        //设置是否抗锯齿
        textPaint.setAntiAlias(true);
    }
}
