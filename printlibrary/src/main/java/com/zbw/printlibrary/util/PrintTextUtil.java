package com.zbw.printlibrary.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;

import java.util.List;

/**
 * Created by Administrator on 2018-06-07.
 * 打印文本拼接工具
 */

public class PrintTextUtil {

    /**
     * 内容在右边，左边补全
     *
     * @param c      补全的字符
     * @param l      需要输出的长度
     * @param string 初始字符串
     * @return 输出目标字符串
     */
    public static String flushRight(char c, long l, String string) {
        if (string == null)
            string = "";
        String str = "";
        String temp = "";
        if (string.length() > l) {
            str = string;
        } else {
            for (int i = 0; i < l - string.length(); i++)
                temp = c + temp;
        }
        str = temp + string;
        return str;
    }

    /**
     * 内容在左边，右边补全
     *
     * @param c      补全的字符
     * @param l      需要输出的长度
     * @param string 初始字符串
     * @return 输出目标字符串
     */
    public static String flushLeft(char c, long l, String string) {
        if (string == null)
            string = "";
        String str = "";
        String temp = "";
        if (string.length() > l)
            str = string;
        else
            for (int i = 0; i < l - string.length(); i++)
                temp = c + temp;
        str = string + temp;
        return str;
    }

    /**
     * 含有数字英文中文的字符串
     *
     * @param str 输入字符串
     * @param num 输出目标长度（按单字节长度来算）
     * @return
     */
    public static String charArithmetic(String str, int num) {
        if (str == null)
            str = " ";
        String srr = "";
        int Num = 0;
        int stt = 0;
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            byte[] bytes = ("" + chars[i]).getBytes();
            if (bytes.length == 1) {
                Num = 1;
            } else {
                Num = 2;
            }
            stt = stt + Num;
            if (num < stt) {
                stt = stt - Num;
                break;
            }
            srr = srr + chars[i];
        }
        if (num > stt) {
            for (int i = 0; i < num - stt; i++)
                srr = srr + " ";
        }
        return srr;
    }

    /**
     * 居中显示
     *
     * @param headStr 需要居中的字符串
     * @param Length  所占空间的长度
     * @return
     */
    public static String headCenter(String headStr, int Length) {
        if (headStr == null)
            headStr = " ";
        String srr = "";
        int Num = 0;
        int stt = 0;
        char[] chars = headStr.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            byte[] bytes = ("" + chars[i]).getBytes();
            if (bytes.length == 1) {
                Num = 1;
            } else {
                Num = 2;
            }
            stt += Num;
        }

        if (Length > stt) {
            for (int i = 0; i < (Length - stt) / 2; i++)
                srr = srr + " ";
            srr += headStr;
        } else {
            srr = headStr;
        }
        return srr;
    }

    public static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
        // Retrieve all services that can matchthe given intent
        PackageManager pm =context.getPackageManager();
        List<ResolveInfo> resolveInfo =pm.queryIntentServices(implicitIntent, 0);

        // Make sure only one match was found
        if (resolveInfo == null ||resolveInfo.size() != 1) {
            return null;
        }

        // Get component info and createComponentName
        ResolveInfo serviceInfo =resolveInfo.get(0);
        String packageName =serviceInfo.serviceInfo.packageName;
        String className =serviceInfo.serviceInfo.name;
        Log.d("tag", "createExplicitFromImplicitIntent: "+packageName);
        ComponentName component = new ComponentName(packageName, className);

        // Create a new intent. Use the old onefor extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);

        // Set the component to be explicit
        explicitIntent.setComponent(component);

        return explicitIntent;
    }


}
