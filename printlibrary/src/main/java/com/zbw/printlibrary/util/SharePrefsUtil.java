package com.zbw.printlibrary.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;


/**
 * SharedPreferences存储数据方式工具类
 * SharedPreference storage utils
 */
public class SharePrefsUtil {
	private static final String TAG = "SharedPreferences";
	protected SharedPreferences pref;

	public void init(Context context) {
		pref = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
	}

	public void init(Context context, String name) {
		pref = context.getSharedPreferences(name, Context.MODE_PRIVATE);
	}

	public void putValue(String key, int value) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return;
		}
		Editor editor = pref.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public void putValue(String key, long value) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return;
		}
		Editor editor = pref.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public void putValue(String key, float value) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return;
		}
		Editor editor = pref.edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	public void putValue(String key, boolean value) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return;
		}
		Editor editor = pref.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public void putValue(String key, String value) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return;
		}
		if (value == null) {
			value = "";
		}
		Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public int getValue(String key, int defValue) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return defValue;
		}
		int value = pref.getInt(key, defValue);

		return value;
	}

	public long getValue(String key, long defValue) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return defValue;
		}
		long value = pref.getLong(key, defValue);

		return value;
	}

	public float getValue(String key, float defValue) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return defValue;
		}
		float value = pref.getFloat(key, defValue);

		return value;
	}

	public boolean getValue(String key, boolean defValue) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return defValue;
		}
		boolean value = pref.getBoolean(key, defValue);

		return value;
	}

	public String getValue(String key, String defValue) {
		if (pref == null) {
			Log.e(TAG, "SharedPreferences not init");
			return defValue;
		}
		String value = pref.getString(key, defValue);

		return value;
	}

	/**
	 * 删除数据
	 */
	public void remove(String key) {
		Editor editor = pref.edit();
		editor.remove(key);
		editor.commit();
	}
}
