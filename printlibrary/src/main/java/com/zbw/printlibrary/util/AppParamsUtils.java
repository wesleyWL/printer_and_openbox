package com.zbw.printlibrary.util;

/**
 * SP操作工具
 * SharedPreference operation utils
 * on 2018/7/30 10:59
 */

public class AppParamsUtils extends SharePrefsUtil {
    private static AppParamsUtils instance = null;

    private AppParamsUtils() {
    }

    public static AppParamsUtils getInstance() {
        if (null == instance) {
            instance = new AppParamsUtils();
        }
        return instance;
    }

    public void setPrintText(String printText) {
        putValue("printText", printText);
    }

    public String getPrintText() {
        return getValue("printText" , null);
    }

    public void setPrintImg(String printImg) {
        putValue("printImg", printImg);
    }

    public String getPrintImg() {
        return getValue("printImg" , null);
    }

    public void setPrintBarcode(String printBarcode) {
        putValue("printBarcode", printBarcode);
    }

    public String getPrintBarcode() {
        return getValue("printBarcode" , null);
    }

    public void setPrintQrcode(String printQrcode) {
        putValue("printQrcode", printQrcode);
    }

    public String getPrintQrcode() {
        return getValue("printQrcode" , null);
    }

    public void setPrintAutoCut(boolean autoCut) {
        putValue("autoCut", autoCut);
    }

    public boolean getPrintAutoCut() {
        return getValue("autoCut" , true);
    }

    public void setPrintLine(int lineCount) {
        putValue("lineCount", lineCount);
    }

    public int getPrintLine() {
        return getValue("lineCount" , 0);
    }

    public void setPrintFont(boolean setPrintFont) {
        putValue("printFont", setPrintFont);
    }

    public boolean getPrintFont() {
        return getValue("printFont" , false);
    }

}
