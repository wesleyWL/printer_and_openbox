package com.zbw.printlibrary.util;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by cyj on 2018-07-01.
 *
 */

public class CheckUtil {

    private static long lastTime;


    /**
     * 判断是横屏还是竖屏 显示
     * @return true: 横屏     false: 竖屏
     */
    public static boolean isHorizontalDisplay(Context context) {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //竖屏
            return false;
        }
        return true;
    }


    /**
     * 验证手机格式
     */
    public static boolean isMobileNo(String mobileNo) {
        /*
        移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
        联通：130、131、132、152、155、156、185、186
        电信：133、153、180、189、（1349卫通）
        总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
        */
        if (TextUtils.isEmpty(mobileNo)) {
            return false;
        }
        if(mobileNo.length() != 11){
            return false;
        }

        //"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        String telRegex = "[1][358]\\d{9}";
        return mobileNo.matches(telRegex);
    }

    /**
     * @param editText  判断editText 内容是否为空,去掉所有空格
     */
    public static boolean isEmpty(EditText editText){
        if(editText == null ){
            return true;
        }
        if(editText.getText().toString().replaceAll(" ","").length() == 0){
            return true;
        }
        return false;
    }

    /**
     * 判断List是否为空
     */
    public static boolean isEmpty(List list){
        return list == null || list.size() == 0;
    }

    /**
     * 判断字符串是否为空
     */
    public static boolean isEmpty(CharSequence content){
        return content == null || content.length() == 0;
    }



    /**
     * 是否是 5连击
     */
    private static int count = 0;
    public static boolean isPenTaKills(){
        if(count == 5){
            count = 0;
            return true;
        }
        long currTime = SystemClock.uptimeMillis();
        if(currTime - lastTime < 1000){
            count++;
        }
        lastTime = SystemClock.uptimeMillis();
        return false;
    }

    /**
     * 获取当前 activity 的截图
     */
    public static void getWindowCompress(View view, String name){
//        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = view.getDrawingCache();

        if (bitmap != null) {
            try {
                String filePath = "/sdcard/" +  name;
                File file = new File(filePath);
                FileOutputStream os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
