package com.zbw.printlibrary.printConnector;

import android.graphics.Bitmap;

import com.zbw.printlibrary.bean.AttributeBean;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by cyj on 2019-07-24.
 */

public interface IPrinterControl {
    /**
     * 连接打印机
     */
    void connectPrinter();

    /**
     * 断开打印机
     */
    void disConnectPrinter();

    /**
     * 打印测试
     */
    void printTest(final List<String> testList);

    void printString(final List<AttributeBean> testList);

    /**
     * @param testList 打印内容
     * @param width 纸张宽度
     */
    void printString(final List<AttributeBean> testList,int width);

    void printStringAndBitmap(final List<String> testList, LinkedHashMap<Integer,Bitmap> bitmapMap);

    void blueConnect(String address);

    void disBlueConnect();


    void openMoneyBox();

    //是否连接状态 true连接中  false断开中
    boolean isConnected();
}
