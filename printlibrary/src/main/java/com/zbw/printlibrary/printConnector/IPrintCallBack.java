package com.zbw.printlibrary.printConnector;

/**
 * Created by cyj on 2019-07-24.
 */

public interface IPrintCallBack {
    /**
     * 连接回调
     */
    void onConnect(String message,boolean b);

    /**
     * 断开回调
     */
    void onDisConnect(String message,boolean b);

    /**
     * 打印结果回调
     * @param resultCode
     */
    void onPrintResult(boolean resultCode);

}
