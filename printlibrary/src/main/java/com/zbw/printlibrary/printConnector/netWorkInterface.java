package com.zbw.printlibrary.printConnector;

/**
 * 作者 ：Wesley
 * 时间 ：2022-10-08 16:51
 * netWorkInterface 说明：
 */
public interface netWorkInterface extends IPrinterControl{
    void setIpAndPort(String ip,String port);

    void setDefaultBuzzer(Boolean isOpen,int number,int time);
}
