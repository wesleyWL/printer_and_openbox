package com.zbw.printlibrary.printConnector;

import android.content.Context;

import com.zbw.printlibrary.openBoxSub.CashBoxK3;
import com.zbw.printlibrary.openBoxSub.CashBoxK5;
import com.zbw.printlibrary.openBoxSub.CashboxPostin;
import com.zbw.printlibrary.openBoxSub.OpenBoxLiandi;
import com.zbw.printlibrary.openBoxSub.YiMinOpenBox;
import com.zbw.printlibrary.printSub.PrintBalance;
import com.zbw.printlibrary.printSub.PrintCPosX5;
import com.zbw.printlibrary.printSub.PrintAclas;
import com.zbw.printlibrary.printSub.PrintBluetooth;
import com.zbw.printlibrary.printSub.PrintChuangXiangY6;
import com.zbw.printlibrary.printSub.PrintESum;
import com.zbw.printlibrary.printSub.PrintFangPai;
import com.zbw.printlibrary.printSub.PrintHisense;
import com.zbw.printlibrary.printSub.PrintJiChengQ1;
import com.zbw.printlibrary.printSub.PrintJiaBoUsb;
import com.zbw.printlibrary.printSub.PrintLKL;
import com.zbw.printlibrary.printSub.PrintLiandi;
import com.zbw.printlibrary.printSub.PrintMeiTuanHardware;
import com.zbw.printlibrary.printSub.PrintMeiTuanM1;
import com.zbw.printlibrary.printSub.PrintMeiTuanS4Sp;
import com.zbw.printlibrary.printSub.PrintNetworkPort;
import com.zbw.printlibrary.printSub.PrintPosIn;
import com.zbw.printlibrary.printSub.PrintRongDa;
import com.zbw.printlibrary.printSub.PrintSangDa;
import com.zbw.printlibrary.printSub.PrintSongDa;
import com.zbw.printlibrary.printSub.PrintSunMi;
import com.zbw.printlibrary.printSub.PrintTianZhiHeF24A;
import com.zbw.printlibrary.printSub.PrintUsb;
import com.zbw.printlibrary.printSub.PrintWld;
import com.zbw.printlibrary.printSub.PrintYiJieTong;
import com.zbw.printlibrary.printSub.PrintYingTai60;
import com.zbw.printlibrary.printSub.PrintZhiBaiChuangB2;
import com.zbw.printlibrary.printSub.PrinterTianBo;

/**
 * Created by cyj on 2019-07-25.
 */

public class NewPrint {
    public static final int PRINT_JIABO_USB = 999;//佳博usb打印
    public static final int PRINT_LANDI = 101;//联迪打印
    public static final int PRINT_SUMMI = 102;//商米打印
    public static final int PRINT_WLD = 103;//微领地打印
    public static final int PRINT_LKL = 104;//拉卡拉打印
    public static final int PRINT_BLE = 105;//蓝牙打印
    //public static final int PRINT_WIFI = 106;//WIFI打印  作废了，请用 PrintNetworkPort 代替
    public static final int PRINT_USB = 107;//USB打印
    public static final int PRINT_SONGDA = 108;//松达打印
    public static final int PRINT_NETWORK = 109;//网口打印
    public static final int PRINT_ACLAS = 111;//顶尖Aclas打印
    public static final int PRINT_ESUM = 112;//易商打印4
    public static final int PRINT_POSIN = 121;//宝盈打印4
    public static final int PRINT_CHUANG_Y6 = 113;//创享-Y6打印
    public static final int PRINT_ZHIBAICHUAN_B2 = 114;//智百川-B2打印
    public static final int PRINT_ZHONGKEYINGTAI_60 = 115;//中科英泰-60打印
    public static final int PRINT_SANGDA = 116;//桑达打印
    public static final int PRINT_JICHENG_Q1 = 117;//吉成-Q1打印
    public static final int PRINT_YIJIETONG = 118;//易捷通打印
    public static final int PRINT_FANGPAI = 119;//方派打印
    public static final int PRINT_TIANZHIHE_F24A = 120;//天之河-F24A打印
    public static final int PRINT_CPOS = 122;//
    public static final int PRINT_TIANBO = 123;//天波打印

    public static final int OPENBOX_LANDI = 151;//联迪开钱箱
    public static final int OPENBOX_POSIN = 152;//宝盈开钱箱
    public static final int PRINT_MEITUAN_M1 = 153;//美团M1
    public static final int PRINT_MEITUAN_S4 = 154;//美团S4
    public static final int PRINT_MEITUAN_HARDWARE = 155;//美团硬件sdk
    public static final int PRINT_BALANCE = 156;//佰伦斯
    public static final int PRINT_HISENSE = 157;//海信

    public static final int OpenBoxYiMin = 158;//一敏

    public static final int PRINT_RONGDA = 159;//容大

    public static final int PRINT_JD_K3 = 160;//京东K3
    public static final int PRINT_JD_K5 = 161;//京东K3

    private static NewPrint mInstance;

    public static int paperSize = 80;


    private NewPrint() {
    }

    public static NewPrint getInstance() {
        if (mInstance == null) {
            synchronized (NewPrint.class) {
                if (mInstance == null) {
                    mInstance = new NewPrint();
                }
            }
        }
        return mInstance;
    }

    /**
     * 设置串口类型
     */
    public NewPrint setChuanKou(String chuanKou) {
        PrintZhiBaiChuangB2.CHUAN_KOU = chuanKou;
        return this;
    }

    /**
     * 设置波特率
     */
    public NewPrint setBaudrate(String baudrate) {
        PrintZhiBaiChuangB2.BAUDRATE = baudrate;
        return this;
    }

    /**
     * 设置USB端口
     */
    public NewPrint setUsbPort(String usbPort) {
        PrintJiChengQ1.USB_PORT = usbPort;
        return this;
    }

    /**
     * 网络打印机支持单独设置ip和端口号
     */
    public netWorkInterface NewNetControl(Context context, IPrintCallBack callback){
        return new PrintNetworkPort(context, callback);
    }

    /**
     *
     * @param context 上下文对象
     * @param callback 回调
     * @param vid vid
     * @param pid pid
     * @return IPrinterControl
     */
    public IPrinterControl NewControlUSB(Context context, IPrintCallBack callback,int vid,int pid,int printType){
        if(printType == PRINT_USB){
            return new PrintUsb(context,callback,vid,pid);
        }else if(printType == PRINT_RONGDA){
            return new PrintRongDa(context,callback,vid,pid);
        }
        return null;
    }


    public IPrinterControl NewControl(int type, Context context, IPrintCallBack callback) {
        IPrinterControl iControl = null;
        switch (type) {
            case PRINT_LANDI:
                iControl = new PrintLiandi(context, callback);
                break;
            case PRINT_SUMMI:
                iControl = new PrintSunMi(context, callback);
                break;
            case PRINT_WLD:
                iControl = new PrintWld(context, callback);
                break;
            case PRINT_LKL:
                iControl = new PrintLKL(context, callback);
                break;
            case PRINT_BLE:
                iControl = new PrintBluetooth(context, callback);
                break;
            case PRINT_USB:
                iControl = new PrintUsb(context, callback);
                break;
            case PRINT_SONGDA:
                iControl = new PrintSongDa(context, callback);
                break;
            case OPENBOX_LANDI:
                iControl = new OpenBoxLiandi(context, callback);
                break;
            case OPENBOX_POSIN:
                iControl = new CashboxPostin(context, callback);
                break;
            case PRINT_NETWORK:
                iControl = new PrintNetworkPort(context, callback);
                break;
            case PRINT_ACLAS:
                iControl = new PrintAclas(context, callback);
                break;
            case PRINT_ESUM:
                iControl = new PrintESum(context, callback);
                break;
            case PRINT_POSIN:
                iControl = new PrintPosIn(context, callback);
                break;
            case PRINT_CHUANG_Y6:
                iControl = new PrintChuangXiangY6(context, callback);
                break;
            case PRINT_ZHIBAICHUAN_B2:
                //可额外设置串口类型和波特率
                iControl = new PrintZhiBaiChuangB2(context, callback);
                break;
            case PRINT_ZHONGKEYINGTAI_60:
                iControl = new PrintYingTai60(context, callback);
                break;
            case PRINT_SANGDA:
                iControl = new PrintSangDa(context, callback);
                break;
            case PRINT_JICHENG_Q1:
                //必须额外设置USB端口
                iControl = new PrintJiChengQ1(context, callback);
                break;
            case PRINT_YIJIETONG:
                iControl = new PrintYiJieTong(context, callback);
                break;
            case PRINT_FANGPAI:
                iControl = new PrintFangPai(context, callback);
                break;
            case PRINT_TIANZHIHE_F24A:
                iControl = new PrintTianZhiHeF24A(context, callback);
                break;
            case PRINT_CPOS:
                iControl = new PrintCPosX5(context, callback);
                break;
            case PRINT_TIANBO:
                iControl = new PrinterTianBo(context, callback);
                break;
            case PRINT_JIABO_USB:
                iControl = new PrintJiaBoUsb(context,callback);
                break;
            case PRINT_MEITUAN_M1:
                iControl = new PrintMeiTuanM1(context,callback);
                break;
            case PRINT_MEITUAN_S4:
                iControl = new PrintMeiTuanS4Sp(context,callback);
                break;
            case PRINT_MEITUAN_HARDWARE:
                iControl = new PrintMeiTuanHardware(context,callback);
                break;
            case PRINT_BALANCE:
                iControl = new PrintBalance(context,callback);
                break;
            case PRINT_HISENSE:
                iControl = new PrintHisense(context,callback);
                break;
            case OpenBoxYiMin:
                iControl = new YiMinOpenBox();
                break;
            case PRINT_RONGDA:
                iControl = new PrintRongDa(context,callback);
                break;
            case PRINT_JD_K3:
                iControl = new CashBoxK3();
                break;
            case PRINT_JD_K5:
                iControl = new CashBoxK5();
                break;
            default:
                break;
        }
        return iControl;
    }
}
