package com.zbw.printlibrary.bean;

/**
 * Created by Administrator on 2019-07-24.
 */

public class PrintBTDevice {
    private String name;
    private String address;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString() {
        return "PrintBTDevice [name=" + name + ", address=" + address + "]";
    }
}
