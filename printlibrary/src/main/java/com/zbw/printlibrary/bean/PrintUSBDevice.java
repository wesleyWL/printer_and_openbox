package com.zbw.printlibrary.bean;

import android.hardware.usb.UsbDevice;

/**
 * 作者 ：Wesley
 * 时间 ：2021/12/14 18:49
 * 这个类是干嘛的？：PrintUSBDevice
 */
public class PrintUSBDevice {
    private String name;
    private UsbDevice mDevice;
    private boolean isCheck;

    public PrintUSBDevice(String name, UsbDevice device, boolean isCheck) {
        this.name = name;
        mDevice = device;
        this.isCheck = isCheck;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UsbDevice getDevice() {
        return mDevice;
    }

    public void setDevice(UsbDevice device) {
        mDevice = device;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\'" + name + "\'" +
                ", \"mDevice\":" + mDevice +
                ", \"isCheck\":" + isCheck +
                '}';
    }
}
