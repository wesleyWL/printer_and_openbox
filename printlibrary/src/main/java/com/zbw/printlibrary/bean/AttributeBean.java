package com.zbw.printlibrary.bean;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * @Author: user
 * @Time: 2021/11/10 14:03
 * @Description: 打印文字属性
 */
public class AttributeBean implements Parcelable {
    //字体大小 0小 1 中 2 大
    private int font_size;
    //字体位置  0左 1 中 2 右
    private int font_gray;
    //是否加粗
    private boolean bold;
    //是否换行
    private boolean font_line=true;
    //内容
    private String context;
    //打印属性 0 文字 1图片 2分割线
    private int context_type;
    //打印图片
    private Bitmap bitmap;

    protected AttributeBean(Parcel in) {
        font_size = in.readInt();
        font_gray = in.readInt();
        bold = in.readByte() != 0;
        font_line = in.readByte() != 0;
        context = in.readString();
        context_type = in.readInt();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<AttributeBean> CREATOR = new Creator<AttributeBean>() {
        @Override
        public AttributeBean createFromParcel(Parcel in) {
            return new AttributeBean(in);
        }

        @Override
        public AttributeBean[] newArray(int size) {
            return new AttributeBean[size];
        }
    };

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public AttributeBean(){

    }
    public AttributeBean(String context){
        this.context=context;
    }

    public AttributeBean(int font_size,int font_gray,String context){
        this.font_size=font_size;
        this.font_gray=font_gray;
        this.context=context;
    }

    public AttributeBean(int font_size,int font_gray,String context,boolean font_line){
        this.font_size=font_size;
        this.font_gray=font_gray;
        this.context=context;
        this.font_line=font_line;
    }

    public AttributeBean(int font_size,int font_gray,String context,int context_type,Bitmap bitmap){
        this.font_size=font_size;
        this.font_gray=font_gray;
        this.context=context;
        this.context_type=context_type;
        this.bitmap=bitmap;
    }


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getContext_type() {
        return context_type;
    }

    public void setContext_type(int context_type) {
        this.context_type = context_type;
    }

    public int getFont_size() {
        return font_size;
    }

    public void setFont_size(int font_size) {
        this.font_size = font_size;
    }

    public int getFont_gray() {
        return font_gray;
    }

    public void setFont_gray(int font_gray) {
        this.font_gray = font_gray;
    }

    public boolean isFont_line() {
        return font_line;
    }

    public void setFont_line(boolean font_line) {
        this.font_line = font_line;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(font_size);
        dest.writeInt(font_gray);
        dest.writeByte((byte) (bold ? 1 : 0));
        dest.writeByte((byte) (font_line ? 1 : 0));
        dest.writeString(context);
        dest.writeInt(context_type);
        dest.writeParcelable(bitmap, flags);
    }
}
