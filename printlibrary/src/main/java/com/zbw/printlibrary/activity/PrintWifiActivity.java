package com.zbw.printlibrary.activity;

import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.zbw.printlibrary.R;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.printConnector.NewPrint;
import com.zbw.printlibrary.util.PrintSharedPreferencesUtils;
import java.util.ArrayList;
import java.util.List;
import static com.zbw.printlibrary.printConnector.NewPrint.PRINT_NETWORK;

public class PrintWifiActivity extends AppCompatActivity implements IPrintCallBack,View.OnClickListener {

    private Button but_cancel, but_print;
    private RelativeLayout back;
    private EditText editText_ip, editText_port;
    private IPrinterControl mIPrinterControl;
    private String editTextIps, editTextPorts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏系统通知栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题栏
        setContentView(R.layout.activity_print_wifi);
       // setActivitySize();
        initUi();
    }

    private void initUi() {
        back = (RelativeLayout) findViewById(R.id.rela_back);
        back.setOnClickListener(this);
        but_cancel = (Button) findViewById(R.id.but_cancel);
        but_cancel.setOnClickListener(this);
        but_print = (Button) findViewById(R.id.but_print);
        but_print.setOnClickListener(this);
        editText_ip= (EditText) findViewById(R.id.editText_ip);
        editText_port= (EditText) findViewById(R.id.editText_port);
        String ip= PrintSharedPreferencesUtils.getParam(this,"editTextIps","");
        String port= PrintSharedPreferencesUtils.getParam(this,"editTextPorts","0");
        editText_ip.setText(ip);
        editText_port.setText(port);
    }

    /**
     * 动态设置Activity的大小。
     */
    private void setActivitySize() {
        //动态设置Activity的大小。
        Display display = getWindowManager().getDefaultDisplay(); // 为获取屏幕宽、高
        Window window = getWindow();
        WindowManager.LayoutParams windowLayoutParams = window.getAttributes(); // 获取对话框当前的参数值
        windowLayoutParams.width = (int) (display.getWidth() * 0.3); // 宽度设置为屏幕的0.7
        windowLayoutParams.height = (int) (display.getHeight() * 0.4); // 高度设置为屏幕的0.7
        windowLayoutParams.alpha = 1f;// 设置透明度
        getWindow().setAttributes(windowLayoutParams);
    }


    @Override
    public void onConnect(String message, boolean b) {

    }

    @Override
    public void onDisConnect(String message, boolean b) {

    }

    @Override
    public void onPrintResult(boolean resultCode) {
        if(!resultCode){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(PrintWifiActivity.this,"连接失败！",Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rela_back) {
            finish();
        } else if (view.getId() == R.id.but_cancel) {
            finish();
        }else if (view.getId() == R.id.but_print) {
            editTextIps=editText_ip.getText().toString().trim();
            editTextPorts=editText_port.getText().toString().trim();
            if (editTextIps.isEmpty()){
                Toast.makeText(this,"IP不能为空",Toast.LENGTH_SHORT).show();
                return;
            }
            if (editTextPorts.isEmpty()){
                Toast.makeText(this,"端口不能为空",Toast.LENGTH_SHORT).show();
                return;
            }

            int prot = Integer.parseInt(editTextPorts);
            if(prot<0||prot>65535){
                Toast.makeText(this,"端口范围 0~65535 ",Toast.LENGTH_SHORT).show();
                return;
            }


            PrintSharedPreferencesUtils.setParam(this,"editTextIps",editTextIps);
            PrintSharedPreferencesUtils.setParam(this,"editTextPorts",editTextPorts);

            mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_NETWORK,PrintWifiActivity.this,PrintWifiActivity.this);
            mIPrinterControl.connectPrinter();
            mIPrinterControl.printTest(printStart());
        }
    }
    private List<String> printStart() {
        List<String> listprine=new ArrayList<>();
        listprine.add("网口打印测试");
        listprine.add("欢迎光临");
        listprine.add("你好");
        listprine.add("打印成功");
        return listprine;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIPrinterControl != null) {
            mIPrinterControl.disConnectPrinter();
        }
    }
}
