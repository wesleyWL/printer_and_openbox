package com.zbw.printlibrary.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zbw.printlibrary.R;
import com.zbw.printlibrary.printConnector.IPrintCallBack;
import com.zbw.printlibrary.printConnector.IPrinterControl;
import com.zbw.printlibrary.printConnector.NewPrint;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.zbw.printlibrary.printConnector.NewPrint.PRINT_BLE;
import static com.zbw.printlibrary.printConnector.NewPrint.PRINT_USB;

public class TestActivity extends AppCompatActivity implements IPrintCallBack {
    private Button btn_connect, btn_print_test,btn_disconnect,btn_open_test,btn_ble_test,btn_ble_use,btn_ble_dis_use;
    private IPrinterControl mIPrinterControl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btn_connect = (Button) findViewById(R.id.btn_connect);
        btn_print_test = (Button) findViewById(R.id.btn_print_test);
        btn_disconnect = (Button) findViewById(R.id.btn_disconnect);
        btn_open_test = (Button) findViewById(R.id.btn_open_test);

        btn_ble_test = (Button) findViewById(R.id.btn_ble_test);
        btn_ble_use = (Button) findViewById(R.id.btn_ble_use);
        btn_ble_dis_use = (Button) findViewById(R.id.btn_ble_dis_use);

        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* //联迪
                mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_LANDI,TestActivity.this,TestActivity.this);
                mIPrinterControl.connectPrinter();*/
               /* //商米
                mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_SUMMI,TestActivity.this,TestActivity.this);
                mIPrinterControl.connectPrint
                er();*/
                //微领地
                mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_USB,getApplicationContext(),TestActivity.this);
                mIPrinterControl.connectPrinter();
//                Log.i("是否连接中：" ,mIPrinterControl.isConnected()+"");
                /*//拉卡拉
                mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_LKL,TestActivity.this,TestActivity.this);
                mIPrinterControl.connectPrinter();*/
               /* //联迪开钱箱
                mIPrinterControl = NewPrint.getInstance().NewControl(OPENBOX_LANDI,TestActivity.this,TestActivity.this);
                mIPrinterControl.connectPrinter();*/

            }
        });
        btn_print_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> testList = new ArrayList<>();
                testList.add("打印测试");
                testList.add("打印测试");
                testList.add("打印测试");
                testList.add("打印测试");
                testList.add("打印测试");
                LinkedHashMap<Integer, Bitmap> map= new LinkedHashMap();
                map.put(0, BitmapFactory.decodeResource(getResources(),R.drawable.qrcode));
                map.put(1, BitmapFactory.decodeResource(getResources(),R.drawable.qrcode));
                map.put(2, BitmapFactory.decodeResource(getResources(),R.drawable.qrcode));
                if (mIPrinterControl != null) {
                    mIPrinterControl.printStringAndBitmap(testList,map);
                }else{
                    Toast.makeText(TestActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIPrinterControl != null) {
                    mIPrinterControl.disConnectPrinter();
                }else{
                    Toast.makeText(TestActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
                }
                Log.i("是否连接中：" ,mIPrinterControl.isConnected()+"");

            }
        });

        btn_open_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIPrinterControl != null) {
                    mIPrinterControl.openMoneyBox();
                }else{
                    Toast.makeText(TestActivity.this,"请先连接",Toast.LENGTH_SHORT).show();
                }
            }
        });


        /**
         * 蓝牙打印测试
         */
        btn_ble_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TestActivity.this, PrintBluetoothActivity.class));
            }
        });

        /**
         * 蓝牙正式测试
         */
        btn_ble_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("蓝牙","==== 1");

                if (mIPrinterControl == null) {
                    mIPrinterControl = NewPrint.getInstance().NewControl(PRINT_BLE, TestActivity.this, TestActivity.this);
                    mIPrinterControl.connectPrinter();
                }

                try {
                    if(mIPrinterControl.isConnected()){
                        mIPrinterControl.printTest(getPrintList());
                    }else{
                        Toast.makeText(TestActivity.this,"打印机连接中，请稍等",Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * 蓝牙断开
         */
        btn_ble_dis_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIPrinterControl != null) {
//                    mIPrinterControl.disConnectPrinter();
                    mIPrinterControl.disConnectPrinter();
                    mIPrinterControl = null;
                }
            }
        });
    }

    private List<String> getPrintList() {
        List<String> printList = new ArrayList<>();
        printList.add("正式打印测试");
        printList.add("正式打印测试");
        printList.add("正式打印测试");
        printList.add("正式打印测试");
        printList.add("正式打印测试");
        return printList;
    }


    @Override
    public void onConnect(final String message, final boolean b) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    Toast.makeText(TestActivity.this,message,Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(TestActivity.this,message,Toast.LENGTH_SHORT).show();
                    mIPrinterControl = null;
                }
            }
        });
    }

    @Override
    public void onDisConnect(final String message, final boolean b) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    Toast.makeText(TestActivity.this,message,Toast.LENGTH_SHORT).show();
                    mIPrinterControl = null;
                }else{
                    Toast.makeText(TestActivity.this,message,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPrintResult(final boolean resultCode) {
        Log.i("打印结果",resultCode+"");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultCode) {
                    Toast.makeText(TestActivity.this,"打印成功",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(TestActivity.this,"打印失败",Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
