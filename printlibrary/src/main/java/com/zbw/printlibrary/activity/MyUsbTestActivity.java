package com.zbw.printlibrary.activity;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zbw.printlibrary.R;
import com.zj.usbsdk.UsbController;

import java.util.HashMap;

public class MyUsbTestActivity extends AppCompatActivity {
    private Button btn_connect, btn_print;
    private UsbManager mUsbManager;
    private PendingIntent permissionIntent;
    private static final String ACTION_USB_PERMISSION = "com.example.android_print.USB_PERMISSION";
    public UsbController usbCtrl;
    public UsbDevice dev;
    private int USB_VID;
    private int USB_PID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_usb_test);
        btn_connect = (Button) findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTest();
            }
        });
        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printStart();
            }
        });
    }


    private void myTest() {
        if(USB_VID!=0||USB_PID!=0){
            dev = usbCtrl.getDev(USB_VID, USB_PID);
        }else {
            mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(
                    ACTION_USB_PERMISSION), 0);
            usbCtrl = new UsbController(this, mHandler);
            HashMap<String, UsbDevice> myDeviceList = mUsbManager.getDeviceList();
            for (UsbDevice myDevice : myDeviceList.values()) {
                dev = usbCtrl.getDev(myDevice.getVendorId(), myDevice.getProductId());
                if (dev != null) {
                    if (!(usbCtrl.isHasPermission(dev))) {
                        // Log.d("usb调试","请求USB设备权限.");
                        usbCtrl.getPermission(dev);
                    }
                    Toast.makeText(this, "已连接上打印机，可以打印测试页", Toast.LENGTH_SHORT).show();

                    USB_VID = myDevice.getVendorId();
                    USB_PID = myDevice.getProductId();
                }
            }
        }
    }

    private void printStart() {
        String msg = "";
        byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x21;
        cmd[2] |= 0x10;
        usbCtrl.sendByte(cmd, dev); // 倍宽、倍高模式
        usbCtrl.sendMsg("　　　　　　　智百威\n", "GBK", dev);
        cmd[2] &= 0xEF;
        usbCtrl.sendByte(cmd, dev); // 取消倍高、倍宽模式
        msg = "  您已经成功的连接上了usb打印机！\n\n" + " 你好 \n\n" + "   打印成功！  \n\n";
        usbCtrl.sendMsg(msg, "GBK", dev);
    }



    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                default:
                    break;
            }
        }
    };
}
