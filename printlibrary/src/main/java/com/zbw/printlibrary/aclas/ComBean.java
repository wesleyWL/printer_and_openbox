package com.zbw.printlibrary.aclas;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 作者：cyj
 * 时间：2020-05-19 14:31
 * 注明：顶尖电子秤
 */
public class ComBean {
    public byte[] bRec = null;
    public String sRecTime = "";
    public String sComPort = "";

    public ComBean(String sPort, byte[] buffer, int size) {
        this.sComPort = sPort;
        this.bRec = new byte[size];

        for(int i = 0; i < size; ++i) {
            this.bRec[i] = buffer[i];
        }

        SimpleDateFormat sDateFormat = new SimpleDateFormat("hh:mm:ss");
        this.sRecTime = sDateFormat.format(new Date());
    }
}
