package com.zbw.printlibrary.adapter;

import android.graphics.Color;
import android.widget.Button;

import androidx.annotation.Nullable;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zbw.printlibrary.R;
import com.zbw.printlibrary.bean.PrintUSBDevice;

import java.util.List;

/**
 * 作者 ：Wesley
 * 时间 ：2021/12/14 18:46
 * 这个类是干嘛的？：PrintDeviceAdapter
 */
public class PrintDeviceAdapter extends BaseQuickAdapter<PrintUSBDevice,BaseViewHolder> {

    public PrintDeviceAdapter() {
        super(R.layout.adapter_usb_select);
    }

    @Override
    protected void convert(BaseViewHolder holder, PrintUSBDevice item) {
        holder.setText(R.id.name_tv,item.getName())
                .addOnClickListener(R.id.select_bt);
        Button button = holder.getView(R.id.select_bt);
        if(item.isCheck()){
            button.setBackgroundResource(R.drawable.print_shape_btn_white_gray);
            button.setTextColor(Color.parseColor("#333333"));
            button.setText("打印");
        }else{
            button.setBackgroundResource(R.drawable.print_shape_btn_blue);
            button.setTextColor(Color.parseColor("#ffffff"));
            button.setText("连接");
        }
    }
}
