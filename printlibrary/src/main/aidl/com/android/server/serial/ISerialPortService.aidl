/* Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.serial;

import android.graphics.Bitmap;

interface ISerialPortService {
    void clearAppCache(in String packageName);
    void clearAppData(in String packageName);

    boolean turnOnPrinter();
    boolean turnOffPrinter();
    boolean openSerialPort();
    void closeSerialPort();
    int getPrinterState();

    int setPrinterLayout(in int type);
    int setPrinterFont(in int type);
    int setPrinterMargin(in int margin);

    int printBuffer(in byte[] data);
    int printUnicodeString(in String data);
    int printBitmap(in Bitmap bmp);
}
