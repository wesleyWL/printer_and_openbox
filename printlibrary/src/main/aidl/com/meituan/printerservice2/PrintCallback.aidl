// PrinterCallback.aidl
package com.meituan.printerservice2;

// Declare any non-default types here with import statements

/**
* 打印结果回调
*/
interface PrintCallback {

     /**
     *打印成功
     */
     void onSuccess();

     /**
     *打印失败
     * @param code
     * @param msg
     */
     void onFailure(int code, String msg);
}
