package com.meituan.printerservice2;

import com.meituan.printerservice2.PrintCallback;

interface PrinterManager {
     /**
     * 打印
     * @param data
     * @param callback @see PrinterCallback
     * 如果成功回调打印成功，失败回调:
     * -1 电源错误; -2 打印机不存在; -3 打印机未连接; -4 正在升级固件; -5 切刀错误; -6 仓盖已打开; -7 缺纸; - 8 过热; -9 重试超时; -10 未知错误
     * 优先级-1>-2>-3>-4>-5>-6>-7->-8>-9>-10
     */
     void print(in byte[] data, PrintCallback callback);

    /**
    * 分单打印
    * @param orderId 订单 id
    * 注:由于 AIDL 数据量的限制，订单数据可能超出限制，提供此接口分段发送数据
    * @param data
    * @param callback @see PrinterCallback
    * 如果成功回调打印成功，失败回调:
    * -1 电源错误; -2 打印机不存在; -3 打印机未连接; -4 正在升级固件; -5 切刀错误; -6 仓盖已打开; -7 缺纸; - 8 过热; -9 重试超时; -10 未知错误
    * 优先级-1>-2>-3>-4>-5>-6>-7->-8>-9>-10
    */
     void printOrder(String orderId, in byte[] data, PrintCallback callback);

     /**
     * 检查打印机是否连接
     * @retrun 是否连接 ture连接 false未连接
     */
     boolean checkPrinterConnection();

     /**
     * 检测是否内置打印机
     * @retrun 是否内置打印机 ture存在打印机 flase不存在
     */
     boolean isPrinterExist();

     /**
     * 获取打印纸类型
     * retrun 获取打印纸类型 0未知 1-58mm 2-80mm
     */
     int getPaperType();

     /**
     * 获取打印机类型
     * @retrun 获取打印机类型 0未知 1串口打印机 2USB打印机
     */
     int getPrinterType();

     /**
     * 获取从第一次打印以来的打印长度，用于计算打印头寿命 * 返回:历史累计打印长度，单位是毫米
     */
     int getPrintedLength();

     /**
     * 获取从第一次打印以来切刀切纸次数
     */
     int getCutPaperTimes();

     /**
     * 获取打印机固件版本号
     * @retrun 打印机固件版本 0未知
     */
     String getFirmwareVersion();

     /**
     * 获取打印机硬件版本号
     * @retrun 打印机硬件版本 0 未知
     */
     String getHardwareVersion();

     /**
     * 获取打印机品牌
     * @retrun 品牌名 默认 meituan
     */
     String getBrandName();

     /**
     * 获取打印机板序列号
     * 返回:打印机板的序列号
     */
     String getPrinterSerialNo();

     /**
     * 获取打印机运行状态，0 正常; -1 电源错误; -2 打印机不存在; -3 打印机未连接; -4 正在升级固件; -5 切刀错误; -6 仓盖已打开; -7 缺纸; - 8 过热; -9 重试超时; -10 未知错误
     * 优先级-1>-2>-3>-4>-5>-6>-7->-8>-9>-10
     */
     int getPrinterStatus();
 }