/*
 * Xpos printservice
 * AIDL Version: 1.0.0
 */

package xprinter.xpos.printservice;

import xprinter.xpos.printservice.ICallback;
import android.graphics.Bitmap;

interface IXposService
{
	/**
	 * 打印机固件升级(只供系统组件调用,开发者调用无效)
	 * @param filepath : 固件文件路径
	 * @callback : 回调函数
	 */
	void updateFirmware(String filepath,in ICallback callback);

	/**
	* 打印机固件状态
	* @return:   0--未知， 1--idle, 2--iap , 3--busy , 4--updating
	*/
	int getFirmwareStatus();

   /**
	* 获取打印机固件版本号
	* @return 成功时返回打印机固件版本号,打印机初始化失败时返回null
	*/
	String getFirmwareVersion();

	/**
	* 获取XposService服务版本
	* @return 版本号，如1.0
	*/
	String getServiceVersion();

	/**
     * 获取服务支持的字体列表
     */
    List<String> getSupportFont();

	/**
	 * 初始化打印机，重置打印机的逻辑程序
	 * @param callback 回调
	 */
	void printerInit(in ICallback callback);

	/**
	* 打印机自检，打印自检内容
	* @param callback 回调
	*/
	void printerSelfChecking(in ICallback callback);

	/**
	 * 打印机走纸(强制换行，结束之前的打印内容后走纸n行)
	 * @param n:	走纸行数
	 * @param callback  回调
	 */
	void lineWrap(int n, in ICallback callback);

	/**
	* 使用原始指令打印
	* @param command   指令
	* @param callback  结果回调
	*/
	void sendCommand(in byte[] command, in ICallback callback);

	/**
	* 设置对齐模式，对之后打印有影响，除非初始化
	* @param alignment:	对齐方式 0--居左 , 1--居中, 2--居右
	* @param callback  结果回调
	*/
	void setAlignment(int alignment, in ICallback callback);

	/**
	* 设置打印字体Typeface, 立即生效，会对缓冲区的数据产生影响，初始化后恢复默认
	* 支持的字符：参考getSupportFont
	* @param typeface:		字体名称
	*/
	void setFontName(String typeface, in ICallback callback);

	/**
	* 设置字体大小, 立即生效，会对缓冲区的数据产生影响，初始化后恢复默认
	* 非标准POS指令
	* @param fontsize:	字体大小
	*/
	void setFontSize(float fontsize, in ICallback callback);

	/**
	* 打印文字，文字宽度满一行自动换行排版，不满一整行不打印除非强制换行
	* @param text:	要打印的文字字符串,必须是utf-8编码
	*/
	void printText(String text, in ICallback callback);

	/**
	* 打印指定字体的文本，字体设置只对本次有效,打印完毕恢复默认字体大小
	* @param text:			要打印文字
	* @param typeface:		字体名称
	* @param fontsize:		字体大小
	*/
	void printTextWithFont(String text, String typeface, float fontsize, in ICallback callback);

	/**
	* 打印表格的一行
	* @param colsTextArr   各列文本字符串数组
	* @param colsWidthArr  各列宽度数组(以英文字符计算, 每个中文字符占两个英文字符, 每个宽度大于0)
	* @param colsAlign	   各列对齐方式(0居左, 1居中, 2居右)
	* 备注: 三个参数的数组长度应该一致, 如果colsText[i]的宽度大于colsWidth[i], 则文本换行
	*/
	void printTableText(in String[] colsTextArr, in int[] colsWidthArr, in int[] colsAlign, in ICallback callback);


	/**
	* 打印图片
	* @param bitmap: 	图片bitmap对象(最大宽度384像素，超过无法打印并且回调callback异常函数)
	*/
	void printBitmap(in Bitmap bitmap, in ICallback callback);

	/**
	* 打印一维条码
	* @param data: 		条码数据
	* @param symbology: 	条码类型
	*    0 -- UPC-A，
	*    1 -- UPC-E，
	*    2 -- JAN13(EAN13)，
	*    3 -- JAN8(EAN8)，
	*    4 -- CODE39，
	*    5 -- ITF，
	*    6 -- CODABAR，
	*    7 -- CODE93，
	*    8 -- CODE128
	* @param height: 		条码高度, 取值1到255, 默认162
	* @param width: 		条码宽度, 取值2至6, 默认2
	* @param textposition:	文字位置 0--不打印文字, 1--文字在条码上方, 2--文字在条码下方, 3--条码上下方均打印
	*/
	void printBarCode(String data, int symbology, int height, int width, int textposition,  in ICallback callback);

	/**
	* 打印二维条码
	* @param data:			二维码数据
	* @param size:	二维码图片大小(单位:点, 小于384 )
	* @param errorlevel:	二维码纠错等级(0 至 3)，
	*                0 -- 纠错级别L ( 7%)，
	*                1 -- 纠错级别M (15%)，
	*                2 -- 纠错级别Q (25%)，
	*                3 -- 纠错级别H (30%)
	*/
	void printQRCode(String data, int size, int errorlevel, in ICallback callback);

	/**
	* 使能缓冲模式，所有打印内容将缓存，调用commitCache()或者disableCache()后打印
	*/
	void enterPageMode();

	/**
    * 打印缓冲区的内容
    */
	void commitPageMode();

	/**
	* 退出缓冲模式
	* @commit 是否打印缓存
	*/
	void exitPageMode(boolean commit);

	/**
     * 设置默认字体，打印机掉电后也不会清除，除非清除应用缓存
     */
    void setDefaultFont(String fontname);
}